import sys
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
try:
    import queue
except ImportError:
    import Queue as queue
import serial
import time
from zmq_client import Client
import os
import re
import serial.tools.list_ports
from SensorListner import ServerThread
Ui_MainWindow, QtBaseClass = uic.loadUiType("MasterGui.ui")

        
class MyApp(QMainWindow):
    def __init__(self):
        super(MyApp, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
	
        self.ui.HighVoltageOn.clicked.connect(self.TurnHighVoltage)
        self.ui.HighVoltageValue.returnPressed.connect(self.SetHighVoltage)

        self.ui.LowVoltageOn.clicked.connect(self.TurnLowVoltage)
        self.ui.LowVoltageValue.returnPressed.connect(self.SetLowVoltage)

		
        self.ui.BreakButton.clicked.connect(self.Break)
        self.ui.PingButton.clicked.connect(self.PingServer)
        
        self.ui.TemperatureValue.returnPressed.connect(self.SetTemperature)
    
        self.ColdJig=Client("10.2.225.251","5556")
        self.ITSDAQ=Client("127.0.0.1","5555")
        self.Master=Client("127.0.0.1","5554")

        self.listner=ServerThread("Sensor Listner")
        self.listner.start()
        self.listner.sig.connect(self.UpdateTest)
	
        self.ConfirmationTestStatus=""
        self.ui.TestTemperature.setText("15")
        self.ui.TestButton.clicked.connect(self.ConfirmationTest)

        self.ui.SensorOn.clicked.connect(self.DataTaking)

        self.ui.HighVoltageChannel.setRange(1,4)
        self.ui.LowVoltageChannel.setRange(1,4)

        self.ui.ReadHighVoltage.clicked.connect(lambda: self.ReadBack("highv,voltage,get"))
        self.ui.ReadHighVoltage.clicked.connect(lambda: self.ReadBack("highv,current,get"))
        
        self.ui.ReadLowVoltage.clicked.connect(lambda: self.ReadBack("lowv,voltage,get, channel "+str(self.ui.LowVoltageChannel.value())))
        self.ui.ReadLowVoltage.clicked.connect(lambda: self.ReadBack("lowv,current,get, channel "+str(self.ui.LowVoltageChannel.value())))

        self.ui.IVButton.clicked.connect(lambda: self.ColdJig.SendServerMessage("do iv curve"))

        self.ui.SetInterlock.clicked.connect(self.SendInterlock)
        
    def Break(self):
        message="break"
        reply=self.ColdJig.SendServerMessage(message)
        self.ITSDAQ.SendServerMessage(message)
        print(reply)
        
    def SetTemperature(self):
        config = self.ui.TemperatureValue.text()
        print("temperature value: "+str(config))
        message="chiller,set,temprature at "+str(config)
        print("sending message: "+message)
        reply=self.ColdJig.SendServerMessage(message)
        print(reply)
        self.ui.ServerReply.setText(reply)

    def PingServer(self):
        png=self.ColdJig.Ping()
        if(png):
            self.ui.ServerReply.setText("server responding")
        else:
            self.ui.ServerReply.setText("Server Not Responding")
	    
    def TurnHighVoltage(self):
        if( self.ui.HighVoltageOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("highV,turn,on")
        else:
            reply=self.ColdJig.SendServerMessage("highV,turn,off")
        self.ui.ServerReply.setText(reply)
  
    def SetHighVoltage(self):
        config = self.ui.HighVoltageValue.text()
        config+=","+str(self.ui.HighVoltageChannel.value())        
        reply=self.ColdJig.SendServerMessage("highV, voltage, "+config)
        self.ui.ServerReply.setText(reply)

    def TurnLowVoltage(self):
        if( self.ui.LowVoltageOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("lowV,turn,on")
        else:
            reply=self.ColdJig.SendServerMessage("lowV,turn,off")
        self.ui.ServerReply.setText(reply)
            
    def SetLowVoltage(self):
        config = self.ui.LowVoltageValue.text()
        config+=" "+str(self.ui.LowVoltageChannel.value())
        print(config)
        reply=self.ColdJig.SendServerMessage("lowV,voltage,set,voltage to "+config)
        self.ui.ServerReply.setText(reply)

    def ConfirmationTest(self):
        message=str("start test at: ")+str(self.ui.TestTemperature.text())
        self.Master.SendServerMessage(message)

    def UpdateTest(self,text):
        self.ConfirmationTestStatus+=text+"\n"
        self.ui.TestStatus.setText(self.ConfirmationTestStatus)
		
    def DataTaking(self):
        if( self.ui.SensorOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("sensors on")
        else:
            reply=self.ColdJig.SendServerMessage("sensors off")
        self.ui.ServerReply.setText(reply)

    def ReadBack(self,message):
        reply=self.ColdJig.SendServerMessage(message)
        if("low" in message.lower()):
            self.ui.LowVoltageReadBack.setText(reply)
        else:
            self.ui.HighVoltageReadBack.setText(reply)

    def SendInterlock(self):
        sht_temp_low=self.ui.sht_temp_low.text()
        sht_temp_high=self.ui.sht_temp_high.text()
        sht_humidity_low=self.ui.sht_humidity_low.text()
        sht_humidity_high=self.ui.sht_humidity_high.text()
        user_ntc_low=self.ui.untc_low.text()
        user_ntc_high=self.ui.untc_high.text()
        hybrid_ntc_high=self.ui.hntc_high.text()
        hybrid_ntc_low=self.ui.hntc_low.text()
        
        
        reply1=self.ColdJig.SendServerMessage("interlock,sht,temp,range of "+str(sht_temp_high)+" to" +str(sht_temp_low))
        time.sleep(.5)
        reply2=self.ColdJig.SendServerMessage("interlock,sht,humidity,range of "+str(sht_humidity_high)+" to" +str(sht_humidity_low))
        time.sleep(.5)
        reply3=self.ColdJig.SendServerMessage("interlock,user ntc,range of "+str(user_ntc_high)+" to" +str(user_ntc_low))
        time.sleep(.5)
        reply4=self.ColdJig.SendServerMessage("interlock,hybrid ntc,range of "+str(hybrid_ntc_high)+" to" +str(hybrid_ntc_low))
        time.sleep(.5)
        reply=reply1+"\n"+reply2+"\n"+reply3+"\n"+reply4
        self.ui.ServerReply.setText(reply)
        
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())

