import sys
from PyQt4 import QtCore, QtGui, uic
try:
    import Queue as queue
except ImportError:
    import queue 
import serial
import time
from zmq_client import Client
import os
import re
import serial.tools.list_ports
from SensorListner import ServerThread
Ui_MainWindow, QtBaseClass = uic.loadUiType("MasterGui.ui")

        
class MyApp(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        
        self.HighVoltageOn.clicked.connect(self.TurnHighVoltage)
        self.HighVoltageValue.returnPressed.connect(self.SetHighVoltage)

        self.LowVoltageOn.clicked.connect(self.TurnLowVoltage)
        self.LowVoltageValue.returnPressed.connect(self.SetLowVoltage)
       

		
        self.BreakButton.clicked.connect(self.Break)
        self.PingButton.clicked.connect(self.PingServer)
        
        self.TemperatureValue.returnPressed.connect(self.SetTemperature)
    
        self.ColdJig=Client("10.2.242.125","5556")
        self.ITSDAQ=Client("127.0.0.1","5555")
        self.Master=Client("127.0.0.1","5554")

        self.listner=ServerThread("Sensor Listner")
        self.listner.start()
        self.listner.sig.connect(self.UpdateTest)
	
        self.ConfirmationTestStatus=""
        self.TestTemperature.setText("15")
        self.TestButton.clicked.connect(self.ConfirmationTest)

        self.SensorOn.clicked.connect(self.DataTaking)

        self.HighVoltageChannel.setRange(1,4)
        self.LowVoltageChannel.setRange(1,4)

        self.ReadHighVoltage.clicked.connect(lambda: self.ReadBack("highv,current,get"))        
        self.ReadTemperature.clicked.connect(lambda: self.ReadBack("chiller,get"))

        #self.ReadLowVoltage.clicked.connect(lambda: self.ReadBack("lowv,voltage,get,channel "+str(self.LowVoltageChannel.value())))
        self.ReadLowVoltage.clicked.connect(lambda: self.ReadBack("lowv,current,get,channel "+str(self.LowVoltageChannel.value())))

        self.IVButton.clicked.connect(lambda: self.ColdJig.SendServerMessage("do iv curve"))

        self.SetInterlock.clicked.connect(self.SendInterlock)
        
    def Break(self):
        message="break"
        reply=self.ColdJig.SendServerMessage(message)
        self.ITSDAQ.SendServerMessage(message)
        print(reply)
        
    def SetTemperature(self):
        config = self.TemperatureValue.text()
        print("temperature value: "+str(config))
        message="chiller,set, temprature at "+str(config)
        print("sending message: "+message)
        reply=self.ColdJig.SendServerMessage(message)
        print(reply)
        self.ServerReply.setText(reply)

    def PingServer(self):
        png=self.ColdJig.Ping()
        if(png):
            self.ServerReply.setText("server responding")
        else:
            self.ServerReply.setText("Server Not Responding")
	    
    def TurnHighVoltage(self):
        if( self.HighVoltageOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("highV,turn,on")
        else:
            reply=self.ColdJig.SendServerMessage("highV,turn,off")
        self.ServerReply.setText(reply)
  
    def SetHighVoltage(self):
        config = self.HighVoltageValue.text()
        if(int(config)>0):
            reply="negative voltage only!"
        else:
            reply=self.ColdJig.SendServerMessage("highV,voltage,set,voltage to"+config)
        self.ServerReply.setText(reply)

    def TurnLowVoltage(self):
        if( self.LowVoltageOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("lowV,turn,on")
            reply2=self.ITSDAQ.SendServerMessage("start_amac")
        else:
            reply2=self.ITSDAQ.SendServerMessage("stop_amac")
            time.sleep(.5)
            reply=self.ColdJig.SendServerMessage("lowV,turn,off")
        self.ServerReply.setText(reply+"\n"+reply2)
            
    def SetLowVoltage(self):
        config = self.LowVoltageValue.text()
        config+=" "+str(self.LowVoltageChannel.value())
        print(config)
        reply=self.ColdJig.SendServerMessage("lowV,voltage,set,voltage to "+config)
        self.ServerReply.setText(reply)

    def ConfirmationTest(self):
        message=str("start test at: ")+str(self.TestTemperature.text())
        self.Master.SendServerMessage(message)

    def UpdateTest(self,text):
        self.ConfirmationTestStatus+=text+"\n"
        self.TestStatus.setText(self.ConfirmationTestStatus)
        self.TestStatus.verticalScrollBar().setValue(self.TestStatus.verticalScrollBar().maximum())
		
    def DataTaking(self):
        if( self.SensorOn.isChecked()):
            reply=self.ColdJig.SendServerMessage("sensors on")
        else:
            reply=self.ColdJig.SendServerMessage("sensors off")
        self.ServerReply.setText(reply)

    def ReadBack(self,message):
        reply=self.ColdJig.SendServerMessage(message)
        if("low" in message.lower()):
            self.LowVoltageReadBack.setText(reply)
        elif('high' in message.lower()):
            self.HighVoltageReadBack.setText(reply)
        elif('chill' in message.lower()):
            self.TemperatureReadBack.setText(reply)

    def SendInterlock(self):
        sht_temp_low=self.sht_temp_low.text()
        sht_temp_high=self.sht_temp_high.text()
        sht_humidity_low=self.sht_humidity_low.text()
        sht_humidity_high=self.sht_humidity_high.text()
        user_ntc_low=self.untc_low.text()
        user_ntc_high=self.untc_high.text()
        hybrid_ntc_high=self.hntc_high.text()
        hybrid_ntc_low=self.hntc_low.text()
        
        
        reply1=self.ColdJig.SendServerMessage("interlock,sht,temp,range of "+str(sht_temp_high)+" to" +str(sht_temp_low))
        time.sleep(.5)
        reply2=self.ColdJig.SendServerMessage("interlock,sht,humidity,range of "+str(sht_humidity_high)+" to" +str(sht_humidity_low))
        time.sleep(.5)
        reply3=self.ColdJig.SendServerMessage("interlock,user ntc,range of "+str(user_ntc_high)+" to" +str(user_ntc_low))
        time.sleep(.5)
        reply4=self.ColdJig.SendServerMessage("interlock,hybrid ntc,range of "+str(hybrid_ntc_high)+" to" +str(hybrid_ntc_low))
        time.sleep(.5)
        reply=reply1+"\n"+reply2+"\n"+reply3+"\n"+reply4
        self.ServerReply.setText(reply)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())

