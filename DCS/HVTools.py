#import matplotlib.pyplot as plt
import numpy as np
import time
import graphyte


def average_current(hv):
    current=0.0
    for i in range(5):
        current+=float(hv.GetCurrent())
        time.sleep(0.1)

    return current/5.0

#check if this should be +1000 or -1000
def IVCurve(hv,grafana_server,step=1, end_limit=-400,begin_limit=-400):
    
    hv.SetVoltageLevel(0)
    hv.ToRear()
    data_points=800
    #data_points=int(abs((int(end_limit)-int(begin_limit))/step))
    current_data=np.zeros(data_points,dtype=float)
    voltage_data=np.zeros(data_points,dtype=float)
    hv.SetVoltageRange(-100)
    graphyte.init(grafana_server)
    #ramp up
    for i in range(data_points):        
        voltage_data[i]=float(hv.GetVoltage())
        current_data[i]=average_current(hv)*1000
        print(current_data[i])
        graphyte.send("QCBox.IV_curve.voltage",voltage_data[i])
        graphyte.send("QCBox.IV_curve.current",current_data[i])
        #if current reached a milliamp something went horribly wrong
        if(current_data[i]>=.0001*1000):
            end_limit=i*step
            curent_data.resize(i,False)
            voltage_data.resize(i,False)
            break
        time.sleep(2)
        #hv.SetVoltageLevel((i+1)*step)
        hv.SetVoltageLevel(-400)  


    #ramp down
    #voltage=end_limit
    #while voltage <=0:
    #    hv.SetVoltageLevel(voltage)        
    #    voltage=voltage-5*step
    #    time.sleep(1)
    #if(voltage !=0):
    #    hv.SetVoltageLevel(0)
    #hv.TurnOff()
    #ax=plt.subplot(111)
    plt.scatter(np.absolute(voltage_data),np.absolute(current_data))
    plt.xlabel('Sensor Voltage')
    plt.ylabel('Sensor Current')
    plt.title('I-V')

    plt.savefig("IVCurve"+str(int(round(time.time())))+".pdf")




