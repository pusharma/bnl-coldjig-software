import serial
from serial import *
import time


class SPS:
    def __init__(self,location="/dev/ttyUSB0"):
        self.chiller=serial.Serial(location,9600,SEVENBITS,parity=PARITY_NONE,stopbits=STOPBITS_ONE,timeout=10)
        
    def SetRampRate(self,rate):
        query="RR="+str(rate)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def SetTemperature(self,temp):
        query="SP="+str(temp)+"\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
        
    def TurnOn(self):
        query="START\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
    
    def TurnOff(self):
        query="STOP\n\r"
        self.chiller.write(query.encode())
        print(self.chiller.readline())
    
    def GetTemperature(self):
        query="PTLOC?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        return float(data[data.index("=")+1:data.index("!")])

    def GetRampRate(self):
        query="RR?\n\r"
        self.chiller.write(query.encode())
        data=self.chiller.readline()
        print(data)
        return float(data[data.index("=")+1:data.index("!")])
