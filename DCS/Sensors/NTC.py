import piplates.DAQCplate as DAQ
import math
from Sensors.Sensor import Sensor
class NTC(Sensor):
        def __init__(self,input_pin=0,resistance=10000,b=3971):
                Sensor.__init__(self,"NTC")
                self.input_pin=int(input_pin)
                self.Ro=float(resistance)
                self.b=float(b)
		
        def Temperature(self):
                print("input pin ",self.input_pin)
                deltav=DAQ.getADC(0,7)
                print(deltav)
                Vin=DAQ.getADC(0,self.input_pin)
                print(Vin)
                To=298.0  #To of NTC
                Ri=self.Ro*math.exp(-self.b/To)
                Rfx=100000

    
                Rn=(Vin*Rfx)/((deltav*2)-Vin)
                T=self.b/(math.log(Rn/Ri))-273
                return T

        def get_data(self):
                return {"temperature":self.Temperature()}

