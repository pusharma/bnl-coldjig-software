import time
import piplates.DAQCplate as DAQ
import numpy as np
from Sensors.Sensor import Sensor

class VacuumSensor(Sensor):
    def __init__(self,input_pin=1):
        Sensor.__init__(self,"PSE541")
        self.input_pin=input_pin
        self.b=1.0 #from data sheet maybe recalibrate
        self.m=0.8 #ditto
        
    def GetPressure(self):
        Vin=DAQ.getADC(0,self.input_pin)
        pressure=(Vin-self.b)/self.m #in kilop
        
        return 0
        
    def GetRaw(self):
        return DAQ.getADC(0,self.input_pin)

    def get_data(self):
        return {"pressure":self.GetPressure()}

