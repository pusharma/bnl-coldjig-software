// ST.cpp    SCT Test Environment:  ROOT User Macro - display and control
// Version PWP 12.09.00
// Version PWP 28.11.00
// Version PWP 13.12.00
// Version PWP 08.03.01
// Version PWP 17.05.01
// Version PWP 18.05.01
// Version PWP 05.07.01  add buttons for IVCurve and HardReset macros
// Version PWP 30.07.01  write username and module temperatures to ROOT file
//                       in easily portable format (standard ROOT objects)
// Version PWP 06.08.01  fix re. output of fitted data objects and 
//                       non-consecutive module numbering
// Version PWP/JGK 08.08.01 merge system test version
//
// Version PWP 04.09.01  Modifications to run with ROOT 3.01/06
//                        in particular, TST object "e" needs to be
//                        specifically added to the object directory.
//
// Version PWP 19.09.01  Changes to handling of canvases:
//                        will now run with either ROOT 2.24 or 3.01
// Version PWP 28.09.01  Additional burst types for use with external trigger
// Version PWP 20.12.01  revised for ROOT 3.02/06; fixed some memory leaks
// Version PWP 21.03.02 add st_set_dbuser
// Version PWP 28.03.02 Revise st_save_scan to increase precision to which 
//                       DCS parameters are recorded
// Version BJG 30.04.02 Fix some variable declarations
// Version PWP 26.11.02 add st_set_idle() and st_kill_idle()
//                      remove st_idle()
// Version PWP 06.11.03 Rename h_hitsLink to h_hitsPerLink
// Version PWP 04.02.04 add st_bell

// This is not needed in some installations of ROOT, but makes sense to keep for all of them
#include <fstream>
#include <vector>

#include "TRint.h"

#ifdef ITSDAQ_ROOT6_DEFINE

#include "ToolBox.h"
#include "STGUICommon.h"
#else
// Shouldn't include source files, but is useful work-around in ROOT5
#include "ToolBox.cpp"
#include "BurstSetup.cpp"
#ifndef ITSDAQ_BUILD_CHECK
#include "TestRegistry.cpp"
#endif
#endif

#include "ST.h"

#include "runtimeCommon.h"

STGUIDisplayCommon *burstDisp;  
STGUIDisplayCommon *scanDisp;

void st_dump_hsio_registers_to_stream(std::ostream &os);
void st_dump_hsio_status_to_stream(std::ostream &os);

void ST(bool onePane) {    // Needs to be same name as file for auto-start

  bool online = true;

  if(getenv("ITSDAQ_OFFLINE")) {
    // i.e. don't try to talk to DAQ board or modules
    online = false;

    std::cout << "Skip DAQ startup\n";
  }

  //gROOT->Reset();

  if(online) {

    // stdll and the TST object are now loaded/declared 
    // within rootlogon.c - but we still create the
    // TST object here.  This approach works for all
    // versions of ROOT (or rather, 2.24 and 3.02/6)
    // PWP 20.12.01

    printf("\nCreating TST object 'e'\n");
    e = new TST();
    printf("...done\n");

    printf("Probing VME system to see if RESMAN needs to be run...\n");
    int ret = e->VMETest();

    if(ret<0){
      printf("Please wait whilst the NIVXI Resource Manager is run");
      if(strncmp(gSystem->GetName(),"Unix",4)==0){   // Linux
        ret = gSystem->Exec("resman");
      }else{
        printf("Resman failed to setup NI resources\n");
        ret = gSystem->Exec("\"c:/Program Files/National Instruments/VXI/resman.exe\"");
      }
      printf("...done\n");

      if(ret == 1) {
        printf("Resman returned %d, won't go any further\n", ret);
        return;
      } else {
        ret=e->VMETest();
        if(ret < 0) {
          printf("VME test still failed, returned %d, won't go any further\n", ret);
          return;
        }
      }
    }

    st_set_dbuser(); // prompt user to enter DB identity PWP 21.03.02

    if(e->UpdateRunInfo() != 0) {
      ExplainDirectoryStructure();
      return;
    }

    // Status with updated run number
    e->PrintStatus();

    e->ConfigureVariable(ST_STREAM_DELAY, 5.0);

    // Uncomment the next line if you want the system to automatically
    // configure the AtlasEndcapReadOut board PWP 07.05.2003
    //e->UseAero();

    printf("\nStarting up SCT system\n");
    e->Startup();
  
    if(e->HccPresent()>0){
      uint16_t fwVersion = e->HsioGetVersion();
      printf("Configuring outsigs for HCC appropriate to fw 0x%04x\n",fwVersion);
      // COM multiplexing on J38, J37, Stave bot, "Stave top / ATLYS"
      e->ConfigureVariable(ST_HSIO_REG_BASE + 22,0x3333);
      // Invert COM multiplexer + enable debug outputs
      e->ConfigureVariable(ST_HSIO_REG_BASE + 23,0x82);
      // Invert COM as it goes out
      e->ConfigureVariable(ST_HSIO_REG_BASE + 18,0x40);
      // Version specific phase adjustments
      if(fwVersion == 0x4434) e->ConfigureVariable(ST_HSIO_REG_BASE + 11,0x88);
      if(fwVersion == 0xb00b) e->ConfigureVariable(ST_HSIO_REG_BASE + 11,0x99);
	
      // Configure Stuff
      e->ExecuteConfigs(-42);
      // reset HCC
      e->HardReset();
      // wait 1000mS
      e->Sleep(1000);
      // Configure again
      e->ExecuteConfigs(-42);
    }

    printf("...done\n");

    e->burst_type = 1;
    e->burst_ntrigs = 50;

    // Next line will turn off the automatic fitting of scans
    // e->do_fits = 0;

    // Next line will turn off the automatic plotting of DCS monitor info
    // e->do_action_DCS = 0;

    // Next line will alter the number of shown modules from the automatic setting
    e->num_showmods = 2;
    e->num_panelmods = 2;

    // New GUI!
    //gROOT->ProcessLine(".L StaveletDisplay.c");

    // Create GUI, so commands can be run manually (HandleMenu)
    burstDisp = STGUI::NewBurst("BurstData");

    st_display_burst();

    if(onePane) {
      scanDisp = burstDisp;
    } else {
      scanDisp = STGUI::NewScan("ScanData");
    }

    st_display_scan();
  }

  // Change the prompt to "ST"

  ((TRint*)gROOT->GetApplication())->SetPrompt("ST [%d] ");

  // Start DCS background monitor
  // st_set_idle();

  std::cout << "ST() ...ready for action!\n";
  if(online) {
    std::cout << "Run number is " << e->runnum << ".\n";
  } else {
    std::cout << "\n";
  }
}

#if 0
void st_menu(){

  TControlBar* bar = new TControlBar("vertical","ST",0,0);
  bar->SetNumberOfColumns(2);

  bar->AddButton("Startup","e->Startup();st_display_dcs()","RELOAD DEFAULT SYSTEM AND MODULE PARAMETERS");
  bar->AddButton("Shutdown","e->Shutdown();st_display_dcs()","Including SCTLV off");
  bar->AddButton("Restart","e->Restart();st_display_dcs()","RELOAD DEFAULT MODULE PARAMETERS");

  // Leave "Run" and "Stop" here because they're jolly useful - PWP 07.08.00
  bar->AddButton("Run","e->RunFree()","Free Running Triggers");
  bar->AddButton("Stop","e->RunStop()","Stop Running Triggers");

  // Simple interactive configs and bursts
  bar->AddButton("ChangeVariable","st_menu_change_variable()","Present the SetVariable panel");
  bar->AddButton("ChangeTrigger","st_menu_change_trigger()","Present the SetTrigger panel");
  bar->AddButton("ExecuteConfigs","e->ExecuteConfigs();st_display_dcs()","Configure modules");
//  bar->AddButton("TriggerBurst","st_menu_trigger_burst()","tburst: read MuSTARD histogram");
//  bar->AddButton("TriggerBurst2","st_menu_trigger_burst2()","tburst2: tries to pinpoint errors");
  bar->AddButton("RawBurst","st_menu_raw_burst()","rawburst: histogram raw data in software");
//  bar->AddButton("DecodedBurst","st_menu_decoded_burst()","Decoded Trigger Burst");
  bar->AddButton("ABCNBurst","st_menu_abcn_burst()","Burst of triggers with software decoding");
//  bar->AddButton("DumpBurst","st_menu_dump_burst()","Decoded Trigger Burst with events dumped to file");
//  bar->AddButton("SendIDBurst","st_menu_sendid_test()","sendidburst: Subtract the expected result from the returned data");
 
  bar->AddButton("RepeatingBurst","st_menu_repeating_burst()","Repeat last used Trigger Burst until Stopped");
 
//  bar->AddButton("ABCD Tests","st_menu_macros()","Hybrid/Module Tests with analysis");
  bar->AddButton("ABCN Tests","st_menu_abcn_tests()","Hybrid/Module Tests for ABCN with analysis");
//  bar->AddButton("B186 Tests","st_menu_systest()","Tests for B186-ST with analysis");

//  bar->AddButton("SimpleScans","st_menu_analogue_scans()","Simple Scans using tburst");
//  bar->AddButton("DigitalScans", "st_menu_digital_scans()", "Scans using sendidburst");
  bar->AddButton("KwikPlot","st_kwikplot()","Show results of integrated fitting for given module");
//  bar->AddButton("FitScan","st_fitscan()","Fit last scan for given module and link");
  bar->AddButton("ShowScurves","st_show_sc()","Display s curves for given module and link");
  bar->AddButton("ShowCounters","st_display_scan_counters()","Show MuSTARD counters for the last scan");

  // Change / Show configurations
  bar->AddButton("ShowSysmap","e->ShowSysmap()","Display current system configuration (sysmap)");
  bar->AddButton("ShowModuleConfig","st_dialog_show_module_config()","Display current settings for a given module");
  bar->AddButton("ShowModuleTrims","st_dialog_show_module_trims()","Display current trims for a given module");
  bar->AddButton("ShowModuleRC","st_dialog_show_module_RC()","Display current RC fits for a given module");
  bar->AddButton("RegisterReadback","st_dump_chip_registers()","Readback registers and dump to console");
  bar->AddButton("FuseIDReadback","st_read_fuse_ids()","Which chips do I have and what are their addresses?");

  // Slow Controls
  // - show buttons only if the system contains appropriate VME modules

  if(e->nSCTHV>0){
    bar->AddButton("HV RampUp",  "e->HVRamp(0);st_display_dcs()", "Ramp UP high voltage");
    bar->AddButton("HV RampDown","e->HVRamp(1);st_display_dcs()", "Ramp DOWN high voltage");
    bar->AddButton("HV Status",  "e->HVStatus();st_display_dcs()","Show status of HV channels");
    bar->AddButton("HV Recovery","e->HVRecoverTrips()","Find and recover from SCTHV trips");
  }

  bar->AddButton("HardReset",  "e->HardReset();st_display_dcs()","Hard reset to all modules");
  
  if(e->nSCTLV>0){
    bar->AddButton("LV On",      "e->LVOn();st_display_dcs()",     "Turn low voltage on");
    bar->AddButton("LV Off",     "e->LVOff();st_display_dcs()",    "Turn low voltage off");
    bar->AddButton("LV Status",  "e->LVStatus();st_display_dcs()", "Show status of LV channels");
    bar->AddButton("LV Recovery","e->RecoverTrips()","Find and recover from SCTLV trips");
  }
  bar->AddButton("DCSQuery","st_dcs_query()", "Query DCS for given module");
  bar->AddButton("DCS->Log","st_dcs_log();st_display_dcs()",   "DCS printout for log-file");

  bar->AddButton("TestPrograms","st_menu_test_programs()","Diagnostic programs");
  bar->AddButton("Status","e->PrintStatus()","System Status");
  bar->AddButton("Documentation","st_docs()","Browse Documentation");
  bar->AddButton("Exit","st_exit()","Power off and Exit ROOT");
  bar->Show();

  gROOT->SaveContext();

}

void st_exit(){
  char c1;
 
  printf("\nDo you really want to exit the CINT/ROOT/ST environment? (y/n)\n");
  scanf("%c",&c1);
  if(c1!='y') return;

  // First turn off SCTLV modules and VcSELs

  e->Shutdown();

  // The following method of exiting ST does so abruptly, without
  // the tedious infinite loop of error messages produced by .q

  gSystem->Exit(0,0);
}

void st_set_idle(){
  printf("Starting DCS monitor\n");
  gROOT->Idle(10, "st_display_dcs();");

  // Run it once to start off with
  st_display_dcs();
}

void st_kill_idle(){
  printf("Stopping DCS monitor\n");
  gROOT->Idle(0, "st_display_dcs();");
}

void st_menu_analogue_scans(){

  TControlBar* abar = new TControlBar("vertical","ST scans");
  abar->SetNumberOfColumns(2);

  // Simple 1D scans using tburst.  Chips in data taking mode.
  // nb: scan loop must be in macro to permit dynamic histogram display updates
  abar->AddButton("Scan",           "st_scan(1,-1)","Repeat Last Scan");
  abar->AddButton("ScanElapsed",    "st_scan(1, 0)","Repeat Trigger Burst under the same conditions");
  abar->AddButton("ScanThreshold",  "st_scan(1, 1)","Scan Threshold in mV");
  abar->AddButton("ScanVCal",       "st_scan(1, 2)","Scan Calibration level in mV");
  //abar->AddButton("ScanQThreshold", "st_scan(1,41)","Scan Threshold in fC");
  abar->AddButton("ScanQCal",       "st_scan(1,42)","Scan Calibration level in fC");
  abar->AddButton("ScanStrobeDelay","st_scan(1, 3)","Scan On Chip Strobe Delay");
  abar->AddButton("ScanPreamp",     "st_scan(1, 4)","Scan Preamplifier Bias current in microA");
  abar->AddButton("ScanShaper",     "st_scan(1, 5)","Scan Shaper current in microA");
  abar->AddButton("ScanStreamDelay","st_scan(1,20)","Relative phase of CLK and DATA at MuSTARD");
  abar->AddButton("ScanClockDuty",  "st_scan(1,23)","Scan Clock Duty Cycle (NOT RECOMMENDED)");
  abar->AddButton("ScanComDelay",   "st_scan(1,25)","n BCOs between Reset and Command (SLOG only)");
  abar->AddButton("ScanPulseDelay", "st_scan(1,26)","n BCOs between Pulse Input and L1A");
  abar->AddButton("ScanTrigDelay",  "st_scan(1,27)","n BCOs between Cal Strobe and L1A ");
  abar->AddButton("ScanVcc",        "st_scan(1,31)","");
  abar->AddButton("ScanVdd",        "st_scan(1,33)","");
  abar->AddButton("ScanVLED",       "st_scan(1,35)","");

  abar->AddButton("ShowScan",       "e->ShowScan()",""); 
  abar->AddButton("ScanMenu",       "e->ScanMenu()","");

  abar->Show();
}

void st_menu_digital_scans(){

  TControlBar *dbar = new TControlBar("vertical","ST");

  // Simple 1D scans using rawburst.  Chips in send ID mode,
  // configs executed before each trigger.  Returned data is
  // compared against the expected result and errors totalled.
  dbar->AddButton("Scan",           "st_scan(6,-1)","Repeat Last Scan"); 
  dbar->AddButton("ScanStreamDelay","st_scan(6,20)","");
  dbar->AddButton("ScanClockDuty",  "st_scan(6,23)","");
  dbar->AddButton("ScanVdd",        "st_scan(6,33)","");
  dbar->AddButton("ScanVLED",       "st_scan(6,35)","");
  dbar->AddButton("ShowScan",       "e->ShowScan()",""); 
  dbar->AddButton("ScanMenu",       "e->ScanMenu()","");

  dbar->Show();
}

void st_menu_test_programs(){

  TControlBar* tbar = new TControlBar("vertical","ST test");

  tbar->AddButton("SCTHV test",   "st_menu_hvtest()","");
  tbar->AddButton("SCTLV test",   "st_menu_lvtest()","");
  tbar->AddButton("TestBurst",    "st_menu_test_burst()","");
  
  tbar->Show();
}

void st_menu_macros(){

  TControlBar* mbar = new TControlBar("vertical","ST Macros");

  if(e->nSCTHV>0){
    mbar->AddButton("IV Curve",         "st_iv()","Record the IV characteristic");
  }
  mbar->AddButton("Set Stream Delay", "st_tm()","Automatic Adjustment of Stream Delay");
  mbar->AddButton("Hard Reset Tests", "st_hr()","Verify Hard Reset Functionality");
  mbar->AddButton("Full Bypass Test", "st_fbt()","Verify Bypass Functionality");
  mbar->AddButton("Redundancy Test",  "st_rt()","Check Redundant Clock and Command");
  mbar->AddButton("Pipeline Test",    "st_pt()","Find dead cells in the pipeline");
  mbar->AddButton("Set Strobe Delay", "st_sd()","Automatic Adjustment of Strobe Delay");
  mbar->AddButton("Three Point Gain", "st_tpg()","Fast measurement of gain and noise");
  mbar->AddButton("Trim Range",       "st_tr()","Trimming, all four ranges");
  mbar->AddButton("Trim Scan",        "st_ts()","Automatic Trimming");
  mbar->AddButton("Response Curve",   "st_rc()","");
  mbar->AddButton("Noise Occupancy",  "st_no()","");
  mbar->AddButton("Timewalk",         "st_tw()","");

  mbar->AddButton("Characterisation Sequence", "st_characterisation()","Full Test Sequence"); 
  mbar->AddButton("Confirmation Sequence",     "st_confirmation()","Short Test Sequence");
 
  mbar->AddButton("Endcap Hybrid LTT","st_eltt()","LTT (cold & warm) of Endcap Hybrids"); 
  mbar->AddButton("Hybrid Warm LTT",  "st_hltt()","Warm Long Term Test of Barrel Hybrids"); 
  mbar->AddButton("Hybrid Cold LTT",  "st_cltt()","Cold Long Term Test of Barrel Hybrids"); 
  mbar->AddButton("Module LTT",       "st_mltt()","Long Term Test of Modules"); 
  mbar->AddButton("IV LTT",           "st_ivltt()","Long Term Test of Detector Leakage Current"); 
 
  mbar->Show();
}

void st_menu_abcn_tests(){

  TControlBar* mbar = new TControlBar("vertical","ABCN Macros");

/*  if(e->nSCTHV>0){
    mbar->AddButton("IV Curve",         "st_iv()","Record the IV characteristic");
  }  */
//  mbar->AddButton("Set Stream Delay", "st_tm()","Automatic Adjustment of Stream Delay");
/*  if(e->GetNEndcaps()>0){  // only show this option if endcap devices have been declared
                           // in the sysmap. PWP 19.11.2003
    mbar->AddButton("Set AERO VDCDAC",  "st_vd()","Automatic Adjustment of AERO VDCDAC");
  } */
  // mbar->AddButton("Hard Reset Tests", "st_abcn_hr()","Verify Hard Reset Functionality");
//  mbar->AddButton("Full Bypass Test", "st_fbt()","Verify Bypass Functionality");
//  mbar->AddButton("Redundancy Test",  "st_rt()","Check Redundant Clock and Command");
//  mbar->AddButton("Pipeline Test",    "st_pt()","Find dead cells in the pipeline");
  mbar->AddButton("Set Strobe Delay", "st_abcn_sd()","Automatic Adjustment of Strobe Delay");
  mbar->AddButton("Three Point Gain", "st_abcn_tpg()","Fast measurement of gain and noise");
  mbar->AddButton("Trim Range",       "st_abcn_tr()","Trimming, all four ranges");
  mbar->AddButton("Response Curve",   "st_abcn_rc()","");
  mbar->AddButton("Noise Occupancy",  "st_abcn_no()","");

  //mbar->AddButton("Timewalk",         "st_abcn_tw()","");
  //mbar->AddButton("NMask",         "st_nmask()","");
  
  mbar->AddButton("Test Sequence",    "st_abcn_test()","SD,3PG,Trim,RC and NO");

/*  mbar->AddButton("Characterisation Sequence", "st_characterisation()","Full Test Sequence"); 
  mbar->AddButton("Confirmation Sequence",     "st_confirmation()","Short Test Sequence");
 
  mbar->AddButton("Endcap Hybrid LTT","st_eltt()","LTT (cold & warm) of Endcap Hybrids"); 
  mbar->AddButton("Hybrid Warm LTT",  "st_hltt()","Warm Long Term Test of Barrel Hybrids"); 
  mbar->AddButton("Hybrid Cold LTT",  "st_cltt()","Cold Long Term Test of Barrel Hybrids"); 
  mbar->AddButton("Module LTT",       "st_mltt()","Long Term Test of Modules"); 
  mbar->AddButton("IV LTT",           "st_ivltt()","Long Term Test of Detector Leakage Current");  */
 
  mbar->Show();
}

void st_menu_systest(){

  TControlBar* stbar = new TControlBar("vertical","ST test");
  stbar->SetNumberOfColumns(1);

  stbar->AddButton("SINGLE 3pt-Gain",   "st_tpg2()", "Single Three Point gain measurement");
  stbar->AddButton("REPEATED 3pt-Gain", "st_svt(0)", "Three Point gain measurements at regular intervals");
  //stbar->AddButton("3pt-G vs t, Strobe","st_svt(1)", "Three Point gain measurements at regular intervals");
  stbar->AddButton("REPEATED N.O.@1fC", "st_novt()", "Noise Occupancy measured at regular intervals");
  stbar->AddButton("SCAN N.O. vs Qthr", "st_no(0,1)","scans noise occupancy vs threshold charge");
  stbar->AddButton("Correlated Noise",  "st_cn()",   "Writes hit pattern to ntuple for correl. noise analysis");
  stbar->AddButton("Common Mode Noise", "st_gamma()","Writes #hits to ntuple for \"Gamma\" analysis");
  stbar->AddButton("INJECTION+scan","st_inj()",  "Single Threshold scan with noise injection");
  stbar->AddButton("INJECTION repeated","st_multiinj()",  "Repeated threshold scans with noise injection");

  stbar->Show();
}

void st_menu_change_trigger(){

  TControlBar* trbar = new TControlBar("vertical","ST trigger");
  trbar->SetNumberOfColumns(1);

  trbar->AddButton("Set ntrigs",    "st_dialog_set_ntrigs()", "No. Triggers per burst"); 
  trbar->AddButton("L1A",           "st_menu_set_trigger(0)", "0 - using CLOAC"); 
  trbar->AddButton("CAL+L1A",       "st_menu_set_trigger(3)", "3 - using CLOAC");
  trbar->AddButton("PULSE+L1A",     "st_menu_set_trigger(20)","20 - using CLOAC");
  trbar->AddButton("Pretrigger+L1A","st_menu_set_trigger(30)","30 - PT, COM_DELAY, L1A"); 
  trbar->AddButton("BCR+L1A",       "st_menu_set_trigger(32)","32 - PT/BCR, COM_DELAY, L1A");
  trbar->AddButton("BCR+CAL+L1A",   "st_menu_set_trigger(34)","34 - PT/BCR, COM_DELAY, CAL, TRIG_DELAY, L1A");
  trbar->AddButton("BCR+PULSE+L1A", "st_menu_set_trigger(36)","36 - PT/BCR, COM_DELAY, CAL, PULSE_DELAY, L1A");
  trbar->AddButton("SR+L1A",        "st_menu_set_trigger(31)","31 - PT/SR, COM_DELAY, L1A");
  trbar->AddButton("SR+CAL+L1A",    "st_menu_set_trigger(33)","33 - PT/SR, COM_DELAY, CAL, TRIG_DELAY, L1A");
  trbar->AddButton("SR+PULSE+L1A",  "st_menu_set_trigger(35)","35 - PT/SR, COM_DELAY, CAL, PULSE_DELAY, L1A");

  trbar->AddButton("USER",          "st_menu_set_trigger(39)","39 - User sequence: file 'sctvar/config/slog.txt'");

  trbar->AddButton("8*L1A",         "st_menu_set_trigger(40)","40 - 8 Consecutive L1A");
  trbar->AddButton("SR+(8*L1A)",    "st_menu_set_trigger(41)","41 - SR, 8 Consecutive L1A");
  trbar->AddButton("SR+CAL+(8*L1A)","st_menu_set_trigger(43)","43 - SR, CAL,8 Consecutive L1A");
  trbar->AddButton("SR+PULSE+(8*L1A)", "st_menu_set_trigger(45)","45 - SR, PULSE,8 Consecutive L1A");

  trbar->AddButton("SR+(n*L1A)",    "st_menu_set_trigger(52)","52 - SR, n spaced L1A");
  trbar->AddButton("SR+CAL+(n*L1A)","st_menu_set_trigger(51)","51 - SR, CAL, n spaced L1A");
  trbar->AddButton("SR+PULSE+(n*L1A)", "st_menu_set_trigger(50)","50 - SR, PULSE,n spaced L1A");

  trbar->AddButton("do_cal_loop",   "st_menu_do_cal_loop(1)", "Loop over four cal lines");
  trbar->AddButton("skip_cal_loop", "st_menu_do_cal_loop(0)", "Single cal line only"); 
  trbar->Show();
}


void st_menu_change_variable(){

  TControlBar* vbar = new TControlBar("vertical","ST variable");
  vbar->SetNumberOfColumns(2);

  vbar->AddButton("SetVThr","st_dialog_change_variable(1)",        "1 - Set Threshold in mV");
  vbar->AddButton("SetVCal","st_dialog_change_variable(2)",        "2 - Set Cal level in mV");
  vbar->AddButton("SetQThr","st_dialog_change_variable(41)",       "41 - Set Threshold in fC");
  vbar->AddButton("SetQCal","st_dialog_change_variable(42)",       "42 - Set Cal level in fC");
  vbar->AddButton("SetStrobeDelay","st_dialog_change_variable(3)", "3 - Set Strobe Delay");
  vbar->AddButton("SetPreamp","st_dialog_change_variable(4)",      "4 - Set Preamplifier Bias in microA");
  vbar->AddButton("SetShaper","st_dialog_change_variable(5)",      "5 - Set Shaper current in microA");
  vbar->AddButton("SetTrimDAC","st_dialog_change_variable(6)",     "6 - Set Trimdac in DAC bits");
  vbar->AddButton("SetCalMode","st_dialog_change_variable(10)",    "10 - Select Calibration line");
  vbar->AddButton("SetCompMode","st_dialog_change_variable(11)",   "11 - Select Data Compression Criteria");
  vbar->AddButton("SetTrimRange","st_dialog_change_variable(12)",  "12 - Select Trim Range");
  vbar->AddButton("SetEdgeDet","st_dialog_change_variable(13)",    "13 - Toggle Edge Detect Mode");
  vbar->AddButton("SetSendMask","st_dialog_change_variable(14)",   "14 - Toggle Send Mask");
  vbar->AddButton("SetAccumulate","st_dialog_change_variable(15)", "15 - Toggle Accumulate bit");
  vbar->AddButton("SetStrmDel","st_dialog_change_variable(20)",    "20 - Relative phase of CLK and DATA at MuSTARD");
  vbar->AddButton("SetClkDuty","st_dialog_change_variable(23)",    "23 - Clock Duty (NOT RECOMMENDED)");
//  vbar->AddButton("SetClkPhase","st_dialog_change_variable(24)",   "24 - Clock Phase (NOT RECOMMENDED)");
  vbar->AddButton("SetComDelay","st_dialog_change_variable(25)",   "25 - n BCOs between Reset and Cal or Pulse command - SLOG only, not CLOAC");
  vbar->AddButton("SetPulseDelay","st_dialog_change_variable(26)", "26 - n BCOs between Pulse Input and L1A");
  vbar->AddButton("SetTrigDelay","st_dialog_change_variable(27)",  "27 - n BCOs between Cal strobe and L1A");
  //vbar->AddButton("SetVDet","st_dialog_change_variable(30)",       "30");  // Not fully implemented ?
  //vbar->AddButton("SetVcc","st_dialog_change_variable(31)",        "31");
  //  vbar->AddButton("SetVi1","st_dialog_change_variable(32)",        "32");  //obsolete
  //vbar->AddButton("SetVPin","st_dialog_change_variable(34)",       "34 - only for SCTLV version 5 or newer");
  //vbar->AddButton("SetVLED","st_dialog_change_variable(35)",       "35");
  //vbar->AddButton("SetSELECT","st_dialog_change_variable(38)",     "38");

  vbar->AddButton("ABCN_CAL_STEP","st_dialog_change_variable(1001)",     "1001 - ST_ABCN_CAL_STEP");
  vbar->AddButton("ABCN_THR_PLUS","st_dialog_change_variable(1002)",     "1002 - ST_ABCN_THRESHOLD_PLUS");
  vbar->AddButton("ABCN_THR_MINUS","st_dialog_change_variable(1003)",    "1003 - ST_ABCN_THRESHOL_MINUS");
  vbar->AddButton("ABCN_L1_LATENCY","st_dialog_change_variable(1004)",   "1004 - ST_ABCN_L1_LATENCY");
  vbar->AddButton("ABCN_L1_COMMAND","st_dialog_change_variable(1005)",   "1005 - ST_ABCN_L1_COMMAND");
  vbar->AddButton("ABCN_BIST_PIPE","st_dialog_change_variable(1006)",    "1006 - ST_ABCN_BIST_PIPELINE");
  vbar->AddButton("ABCN_BIST_DERND","st_dialog_change_variable(1007)",   "1007 - ST_ABCN_BIST_DERANDOMISER");
  vbar->AddButton("ABCN_SHAPER_FBCK","st_dialog_change_variable(1008)",  "1008 - ST_ABCN_SHAPER_FEEDBACK");
  vbar->AddButton("ABCN_PREAMP_FBCK","st_dialog_change_variable(1009)",  "1009 - ST_ABCN_PREAMP_FEEDBACK");
  vbar->AddButton("ABCN_PREAMP_BUFF","st_dialog_change_variable(1010)",  "1010 - ST_ABCN_PREAMP_BUFFER");
  vbar->AddButton("ABCN_DIFF_CURRENT","st_dialog_change_variable(1011)", "1011 - ST_ABCN_DIFFERENTIAL_CURRENT");
  vbar->AddButton("ABCN_COMP_CURRENT","st_dialog_change_variable(1012)", "1012 - ST_ABCN_COMPARATOR_CURRENT");
  vbar->AddButton("ABCN_DRIVE_UP","st_dialog_change_variable(1013)",     "1013 - ST_ABCN_DRIVE_UP");
  vbar->AddButton("ABCN_DRIVE_DOWN","st_dialog_change_variable(1014)",   "1014 - ST_ABCN_DRIVE_DOWN");
  
  vbar->Show();
}
 
void st_dialog_change_variable(short typ){
  float var;
  char nam[32];

  e->TranslateVariableName(typ,nam);
  printf("Change %s to  (float)  ?", nam);
  scanf("%f",&var);
  printf("\nChanging variable type %d %s to %f\n", typ,nam,var);
  e->ConfigureVariable(typ,var);
  e->ExecuteConfigs();
  //st_display_dcs();
}
#endif

void st_dialog_show_module_config(){
  int imod;

  printf("Show config for module (int) ?");
  scanf("%d",&imod);
  e->ShowModule(imod);
}

void st_dialog_show_module_trims(){
  int imod;

  printf("Show trims for module (int) ?");
  scanf("%d",&imod);
  e->ShowTrims(imod);
}

void st_dialog_show_module_RC(){
  int imod;

  printf("Show Response Curve fits for module (int) ?");
  scanf("%d",&imod);
  e->ShowRC(imod);
}

void st_menu_btest(){

  gSystem->Exec("btest");
}

void st_menu_ctest(){

  gSystem->Exec("ctest");
}

void st_menu_mtest(){

  gSystem->Exec("mtest");
}

void st_menu_otest(){

  gSystem->Exec("otest");
}
void st_menu_hvtest(){

  gSystem->Exec("hvtest");
}

void st_menu_lvtest(){

  gSystem->Exec("lvtest");
}

void st_menu_stest(){

  gSystem->Exec("stest");
}

void st_dialog_set_ntrigs(){
  int n;

  printf("Requested number of triggers per burst(int) ?");
  scanf("%d",&n);
  e->burst_ntrigs=n;
}

void st_menu_set_trigger(short trtype){
  e->burst_trtype=trtype;
  printf("Trigger type %hd selected\n",trtype);
  
  if (trtype==39){
    printf("User sequence selected.  How many events per loop?(short) ?");
    int n;
    scanf("%d",&n);
    e->L1A_per_loop = n;
  } else if ((trtype>39)&&(trtype<50)){
    e->L1A_per_loop = 8;
  } else if (trtype>49){
      printf("Multiple SLOG L1A selected.  How many L1A per loop?(short, 1-8) ?");
      int n;
      scanf("%d",&n);
      e->L1A_per_loop = n;
      e->ConfigureVariable(ST_NL1A, n);
      printf("                             Spacing between L1A (short, 0-1024) ?");
      scanf("%d",&n);
      e->ConfigureVariable(ST_L1A_SPACING, n);
      printf("                             How many L1A do you wish to ignore (short, 0-%d) ?",(e->L1A_per_loop-1));
      scanf("%d",&n);
      e->throw_away = n;
  }
  else e->L1A_per_loop = 1;
}

void st_menu_trigger_burst(){
  int n;
  e->burst_type = 1;
  e->ExecuteBurst();
  st_display_burst();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_trigger_burst2(){
  int n;
  e->burst_type = 11;
  e->ExecuteBurst();
  st_display_burst();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_vburst(){
  int n;
  e->burst_type = 12;
  e->ExecuteBurst();
  st_display_burst();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_eburst(){
  int n;
  e->burst_type = 20;
  e->ExecuteBurst();
  st_display_burst();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_decoded_eburst(){
  int n;
  e->burst_type = 21;
  e->ExecuteBurst();
  st_display_burst();
  st_display_burst2();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_dump_burst(){
  int n;
  short old_burst = e->burst_type;
  short old_debug = e->debug_level;
  short old_loop  = e->do_cal_loop;

  e->burst_type=100;
  e->debug_level=999;
  e->do_cal_loop=0;

  e->ExecuteBurst();
  st_display_burst();

  e->burst_type=old_burst;
  e->debug_level=old_debug;
  e->do_cal_loop=old_loop;

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_dump_vburst(){
  int n;
  short old_burst = e->burst_type;
  short old_debug = e->debug_level;
  short old_loop  = e->do_cal_loop;

  e->burst_type=102;
  e->debug_level=999;
  e->do_cal_loop=0;

  e->ExecuteBurst();
  st_display_burst();

  e->burst_type=old_burst;
  e->debug_level=old_debug;
  e->do_cal_loop=old_loop;

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_decoded_burst(){
  int n;
  e->burst_type = 100;
  e->ExecuteBurst();
  st_display_burst();
  if(e->RetrieveVariable(0, 200) < 8) {
    // Only really works for ABCD so far
    //   Starting ABC130, cluster size only
    st_display_burst2();
  }

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_decoded_vburst(){
  int n;
  e->burst_type = 102;
  e->ExecuteBurst();
  st_display_burst();
  st_display_burst2();

  for (n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_test_burst(){
  e->burst_type = 111;
  e->ExecuteBurst();
  e->num_showmods = 5;
  st_display_burst();
}

void st_menu_do_cal_loop(short val){
 
  e->do_cal_loop=val;
  printf("e->do_cal_loop=%d\n",e->do_cal_loop);
}

void st_menu_raw_burst(){

  e->burst_type = 5;
  e->ExecuteBurst();
  st_display_burst();

  for (Int_t n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_abcn_burst(){
  e->burst_type = 8;
  e->ExecuteBurst();
  st_display_burst();

  for (Int_t n=0; n<e->num_showmods; n++){
    if(e->m[n]->present) e->m[n]->PrintBurst();
  }
}

void st_menu_sendid_test(){
  e->ConfigureVariable(ST_DTM, 0);  // configure sendID mode
  e->ExecuteConfigs();         // execute configs (which in sendID mode omits goto_datataking)
  e->burst_type = ST_SENDIDBURST;  // do type 6 burst
  e->ExecuteBurst();
  st_display_burst();          // display burst
  e->ConfigureVariable(ST_DTM, 1);  // configure sendDATA mode
  e->ExecuteConfigs();         // execute configs (now appending goto_datataking for normal running) 
}

void st_menu_repeating_burst(int burst_type) {
  int stop_loop = 0;

  int save_burst_type = e->burst_type;
  e->burst_type = burst_type;

  e->abort=0;
  while (!stop_loop){
    e->ExecuteBurst();
    st_display_burst();

    for (Int_t n=0; n<e->num_showmods; n++){
      if(e->m[n]->present) e->m[n]->PrintBurst();
    }

    stop_loop=st_do_stop();
  }
  e->burst_type = save_burst_type;
}

// Return  0 for success
int st_decode_abcn_chip_register(int mid, int l, int c, unsigned char *address, unsigned int *regValue) {
  int accumulate = 0;

  int offset = 0;

  int scan_width = e->m[mid]->scan_size(e->burst_count, l);
  // Skip 0s at the beginning
  while(!e->m[mid]->scan_lookup(e->burst_count, l, offset)) {
    offset ++;
    if(offset > 100 || offset >= scan_width)
      return 1;
  }

//   printf("Skip %d bits\n", offset);

//   printf("Header: ");
//   for(int b=0; b<18; b++) {
//     int bit = e->m[mid]->scan[e->burst_count][l][offset + b];
//     printf("%d", bit);
//   }
//   printf("\n");

  // Ignore header
  // 11101 0 LLLL BBBBBBBB
  offset += 18;

  // Readback register data (a - chip address, r - register, d - data)
  // 000 aaaaaaa 010 rrrrr dddddddd 1 dddddddd 1
  for(int b=0; b<36; b++) {
    if((offset + c*36 + b) >= scan_width) break;

    int bit = e->m[mid]->scan_lookup(e->burst_count, l, offset + c*36 + b);

    accumulate = (accumulate << 1) + bit;

    // printf("reg acc: b = %d, %d, %d\n", b, bit, accumulate);
    switch(b) {
    // Reset before marker and reg 8-15
    case 0: case 27: accumulate = 0; break;
    case 3: {
      // Marker should be 0 = err/config/reg readback
      if(accumulate != 0) return 1;
      }
      break;
    case 10: *address = accumulate; accumulate = 0; break;
    case 13:
      {
        // 2 means reg readback
        if(accumulate != 2) return 2;
      }
      break;
    case 18: // ignore reg address
      accumulate = 0;
      break;
    case 26: *regValue |= (accumulate & 0xff) << 8; accumulate = 0; break;
    case 35: *regValue |= (accumulate & 0xff); accumulate = 0; break;
    default: break;
    }
  }

  return 0;
}

// Return 0 for success
//        1 if packet not found
//        2 if packet corrupt
int st_decode_abc130_chip_register(int id, int l, int c, unsigned char *address, unsigned char *regAddress, unsigned int *regValue) {
  unsigned int accumulate = 0;

  // Can probably be 0
  int offset = 4;

  int scan_width = e->m[id]->scan_size(e->burst_count, l);

  // Skip 0s at the beginning
  while(!e->m[id]->scan_lookup(e->burst_count, l, offset)) {
    offset ++;
    if(offset >= scan_width)
      return 1;
  }

  int hccStyle = e->HccPresent();

  if(hccStyle) {
    for(int skip=0; skip<c; skip++) {
      offset += 66; // Skip over previous chip
      if(offset >= scan_width)
        return 1;

      while(!e->m[id]->scan_lookup(e->burst_count, l, offset)) {
        offset ++;
        if(offset >= scan_width)
          return 1;
      }
    }
  } else {
    offset += c*66; // not 64???
  }

  if(e->debug_level > 1) {
    printf("Using offset %d for %d %d %d\n", offset, id, l, c);
  }

  if(offset + 70 >= scan_width) {
    // Not enough bits captured
    return 1;
  }

  if(e->debug_level > 5) {
    for(int b=0; b<70; b++) {
      if((b+offset) >= scan_width) { break;}
      int bit = e->m[id]->scan_lookup(e->burst_count, l, offset + b);
      printf("%d", bit);
      if((!hccStyle && ((b == 4) || (b == 9))) ||
         ( hccStyle && ((b == 4) || (b == 9) || (b == 13) || (b == 21) || (b == 29) || (b == 61)))) printf(" ");
    }
    printf("\n");
  }

  if(hccStyle) {
    // HCC inserts 4 bits in header after start bit
    offset += 4;
  }

  for(int b=0; b<58; b++) {
    if((b+offset) >= scan_width) { break; }
    int bit = e->m[id]->scan_lookup(e->burst_count, l, offset + b);

    accumulate = (accumulate << 1) + (bit?1:0);

    // printf("reg acc: b = %d, %d, %d\n", b, bit, accumulate);
    switch(b) {
    case 0:
      // Start bit
      //  NB, for HCC different place, but also we've already started from first 0!
      if(!hccStyle && accumulate == 0) { printf("Start bit %d ", accumulate); return 1; }
      accumulate = 0;
      break;
    case 5: *address = accumulate; accumulate = 0; break; // Chip id
    case 9:
      // Type (reg readback)
      if(accumulate != 8 && accumulate != 13) {
        printf("Type %d != 8 (ABC130) or 13 (HCC) ", accumulate);
        return 2;
      }
      accumulate = 0;
      break;
    case 17: *regAddress = accumulate; accumulate = 0; break; // L0ID
    case 25: accumulate = 0; break; // BCID
    case 25+32: *regValue = accumulate; accumulate = 0; break; // Value
    default: break;
    }
  }

  return 0;
}

void st_dump_chip_registers() {
  int doABC130 = 0;
  int doABCN = 0;
  for (int n=0; n<e->GetMaxModules(); n++){
    if(e->m[n]->present) {
      int chipset = e->RetrieveVariable(n, 200);
      if(chipset == 6) { doABCN = 1; }
      if(chipset == 7) { doABC130 = 1; }
    }
  }

  if(doABC130 && doABCN) {
    printf("Warning: There will probably be confusion when we send the ABC130 code to ABCN and vice versa\n");
  }

  if(doABC130) {
    bool includeMasks = false;
    st_dump_abc130_chip_registers(includeMasks, e->HccPresent()>0);
  }

  if(doABCN) {
    st_dump_abcn_chip_registers();
  }
}

void st_dump_hcc_chip_registers() {
  printf("HCC registers\n");
  for(int regId=0; regId<37; regId++) {
    if(regId > 17 && regId < 32) continue;
    e->ReadChipRegister(regId + 256); // ie HCC register!

    unsigned char chipAddress;
    unsigned char regAddress;
    unsigned int regValue;
    for (int n=0; n<e->GetMaxModules(); n++){
      if(e->m[n]->present) {
        // HCC registers only come on one of the streams, but not sure which
        int c=0;
        while(1) {
          int retVal = st_decode_abc130_chip_register(n, 0, c++, &chipAddress, &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
          switch(regAddress){
            case 32:
              printf(" chip %d  addr %2d value %08x AM3 0d%04d AM2 0d%04d AM1 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x03ff00000)>>20, (regValue & 0xffc00)>>10, (regValue & 0x003ff) );
              break;
            case 33:
              printf(" chip %d  addr %2d value %08x AM6 0d%04d AM5 0d%04d AM4 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x03ff00000)>>20, (regValue & 0xffc00)>>10, (regValue & 0x003ff) );
              break;
            case 34:
              printf(" chip %d  addr %2d value %08x AMil 0x%01x AMwh 0x%02x AMwl 0x%02x AM7 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x0f000000)>>24, (regValue & 0xfe0000)>>17, (regValue & 0x1fc00)>>10, (regValue & 0x003ff) );
              break;
            default:
              printf(" chip %d  addr %2d value %08x\n", chipAddress, regAddress, regValue);
              break;
          } 
        }
        c=0;
        while(1) {
          int retVal = st_decode_abc130_chip_register(n, 1, c++, &chipAddress, &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
          switch(regAddress){
            case 32:
              printf(" chip %d  addr %2d value %08x AM3 0d%04d AM2 0d%04d AM1 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x03ff00000)>>20, (regValue & 0xffc00)>>10, (regValue & 0x003ff) );
              break;
            case 33:
              printf(" chip %d  addr %2d value %08x AM6 0d%04d AM5 0d%04d AM4 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x03ff00000)>>20, (regValue & 0xffc00)>>10, (regValue & 0x003ff) );
              break;
            case 34:
              printf(" chip %d  addr %2d value %08x AMil 0x%01x AMwh 0x%02x AMwl 0x%02x AM7 0d%04d\n", 
                chipAddress, regAddress, regValue,
                (regValue & 0x0f000000)>>24, (regValue & 0xfe0000)>>17, (regValue & 0x1fc00)>>10, (regValue & 0x003ff) );
              break;
            default:
              printf(" chip %d  addr %2d value %08x\n", chipAddress, regAddress, regValue);
              break;
          } 
        }
      }
    }
  }
}

void st_hcc_am_read(bool enable, bool verbose) {
  // List of registers which need to be read
  unsigned char regList[] = {2, 3, 4, 5, 6, 7, 8, 32, 33, 34};
  
  unsigned int *data = new unsigned int [10 * e->GetMaxModules()];
  memset((void*)data, 0, 10 * e->GetMaxModules() * sizeof(unsigned int) );
  
  unsigned char *hccId = new unsigned char [e->GetMaxModules()];
  memset((void*)hccId, 0, e->GetMaxModules() * sizeof(unsigned char) );
  
  if(enable){
    e->ConfigureVariable(ST_HCC_AM_ENABLE_CLK, 1);
    e->ExecuteConfigs();
    e->Sleep(500);
  }
  
  // Now read the register values
  for(int i=0; i<10; i++) {
    e->ReadChipRegister(regList[i] + 256); // ie HCC register!

    for (int n=0; n<e->GetMaxModules(); n++){
	  unsigned char regAddress = 0;
      unsigned int regValue = 0;
	  
      if(e->m[n]->present) {
        // HCC registers only come on one of the streams, could be either
        int c=0;
        while(regAddress==0) {
          int retVal = st_decode_abc130_chip_register(n, 0, c++, &hccId[n], &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
        }
        c=0;
        while(regAddress==0) {
          int retVal = st_decode_abc130_chip_register(n, 1, c++, &hccId[n], &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
        }
		for(int j=0; j<10; j++){
		  if(regAddress==regList[j]){
		   data[(n*10)+j] = regValue;
		   break;
          }
        }
      }
    }
  }
  
  printf("st_hcc_am_read: WARNING, calibration is preliminary!\n");
  
  for (int n=0; n<e->GetMaxModules(); n++){
    if(e->m[n]->present) {
      st_hcc_am_decode(n, hccId[n], &data[(n*10)], verbose);
	}
  }
  
  if(enable){
    e->ConfigureVariable(ST_HCC_AM_ENABLE_CLK, 0);
    e->ExecuteConfigs();
  }

  delete [] data;
  delete [] hccId;
}

void st_hcc_am_decode(int n, unsigned char hccId, unsigned int *regData, bool verbose) {

  uint16_t amLo[7];
  uint16_t amHi[7];
  uint16_t amLt[7];
  float    amMv[7];
  double   amGn = 0.7865; // mV per count based upon crude measurement of one die at RAL
  float    am7Max[] = { 50, 500, 2500, 5000}; // Approximate full scale (in uA) for each of four Idet ranges
  const char * amNames[] = {"Bandgap", "VddRaw", "VddReg", "Vsense", "Tntc", "Tdiode", "Idet", "ExtGnd"};  

  // Register 2
  bool am4atten = (regData[0]&0x0800);
  bool am1sel   = (regData[0]&0x0400);
  unsigned int am7range = (regData[0]&0x0300)>> 8;
  
  // Register 3
  amLo[0] = (regData[1]&0x000003ff) >>  0;
  amHi[0] = (regData[1]&0x000ffc00) >> 10;
  amLo[1] = (regData[1]&0x3ff00000) >> 20;
  
  // Register 4
  amHi[1] = (regData[2]&0x000003ff) >>  0;
  amLo[2] = (regData[2]&0x000ffc00) >> 10;
  amHi[2] = (regData[2]&0x3ff00000) >> 20;
  
  // Register 5
  amLo[3] = (regData[3]&0x000003ff) >>  0;
  amHi[3] = (regData[3]&0x000ffc00) >> 10;
  amLo[4] = (regData[3]&0x3ff00000) >> 20;
  
  // Register 6
  amHi[4] = (regData[4]&0x000003ff) >>  0;
  amLo[5] = (regData[4]&0x000ffc00) >> 10;
  amHi[5] = (regData[4]&0x3ff00000) >> 20;
  
  // Register 7
  amLo[6] = (regData[5]&0x000003ff) >>  0;
  amHi[6] = (regData[5]&0x000ffc00) >> 10;
  
  // Register 32
  amLt[0] = (regData[7]&0x000003ff) >>  0;
  amLt[1] = (regData[7]&0x000ffc00) >> 10;
  amLt[2] = (regData[7]&0x3ff00000) >> 20;
  
  // Register 33
  amLt[3] = (regData[8]&0x000003ff) >>  0;
  amLt[4] = (regData[8]&0x000ffc00) >> 10;
  amLt[5] = (regData[8]&0x3ff00000) >> 20;
  
  // Register 34
  amLt[6] = (regData[9]&0x000003ff) >>  0;
  
  // I found this in the code base we used to test AMAC]
  // perhaps we should not throw this away PWP 08/03/2018
  // trick: assume VDDRAW is really 1450mV
  // Calibrate all other readings based upon this.
  // amGn = 725 / (double) amLt[1];
  
  // Scale voltage measurements to (approximate) mV
  amMv[0] = amLt[0] * amGn;
  amMv[1] = amLt[1] * amGn * 2;
  amMv[2] = amLt[2] * amGn * 3/2;
  if(am4atten) amMv[3] = amLt[3] * amGn * 3/2;
  else         amMv[3] = amLt[3] * amGn;
  amMv[4] = amLt[4] * amGn;
  amMv[5] = amLt[5] * amGn;
  
  // Scale sensor current to (approximate) uA 
  amMv[6] = amLt[6] * (am7Max[am7range] / 1024.0);
 
  
  // NTC is Panasonic ERTJ1VT102J
  // http://industrial.panasonic.com/ww/products/thermal-solutions/ntc-thermistor-chip-type/ntc-thermistor/ntc-thermistorchip-type/ERTJ1VT102J
  
  // NTC constants
  double beta = 4500; // Barrel (Panasonic ERTJ1VT102J) is 4500, EC is 3650
  double rinf = 1000 * TMath::Exp(- beta/298.15);
  double biasR = 1000; // at least true for barrel hybrids
  double biasV = 600; // Assume we use regulated VDD/2
  
//   double ntc_r = biasR * amMv[4] / (biasV -amMv[4]);
  double ntc_r = biasR * (biasV / amMv[4] - 1.0);
  double ntc_t = (beta / TMath::Log(ntc_r/rinf)) -273;

  if(verbose)
    printf("Decoding AM data for hybrid %d HCC address %d:\n", n, hccId);
  
  if(verbose)
    printf(" Channel\tRaw\tmV\tlo\thi\tr\tt\n");
  for(int j=0; j<7; j++){
    int label=j;
    if((j==0)&&(am1sel==true)) label =7;
    if(verbose) {
      printf(" AM%d %s\t%04d\t%0.2f\t%04d\t%04d",
             j+1, amNames[label], amLt[j], amMv[j], amLo[j], amHi[j]);
      if(j==4){
        printf("\t%2.1f\t%2.1f\n",ntc_r, ntc_t);
      }else{
        printf("\n");
      }
    }

    // Store cooked data to array
    e->m[n]->DCS->amValues[j] = amMv[j];
    
    // Store raw data to tree
    e->m[n]->DCS->Record(Form("HCC_%s", amNames[label]), amLt[j]);
  }
  e->m[n]->DCS->Store();
}

void st_dump_abc130_chip_registers(int dump_masks, bool include_hcc) {
  int regIds[] = {0, 1, 2, 3, 6, 7,
                  32, 33, 34, 35,
                  48, 49, 50, -1};
  std::string regNames[] = {"DCS0", "DCS1", "DCS2", "DCS3", "DCS6", "DCS7",
                            "Config0", "Config1", "Config2", "Config3",
                            "SEUStat", "FIFOStat", "L0ID", ""};

  for(int regIndex = 0; regIds[regIndex] != -1; regIndex ++ ) {
    int regId = regIds[regIndex];
//	e->burst_count = 0;
    e->ReadChipRegister(regId);
    printf("Register %d: %s\n", regId, regNames[regIndex].c_str());

    unsigned char chipAddress;
    unsigned char regAddress;
    unsigned int regValue;
    for (int n=0; n<e->GetMaxModules(); n++){
      if(e->m[n]->present) {
        int c=0;
        while(1) {
          int retVal = st_decode_abc130_chip_register(n, 0, c++, &chipAddress, &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
          printf(" chip %d  addr %2d value %08x\n", chipAddress, regAddress, regValue);
        }
        c=0;
        while(1) {
          int retVal = st_decode_abc130_chip_register(n, 1, c++, &chipAddress, &regAddress, &regValue);
          if(retVal) { if(retVal == 2) printf("Parsing failed\n"); break; }
          printf(" chip %d  addr %2d value %08x\n", chipAddress, regAddress, regValue);
        }
      }
    }
  }

  if(dump_masks) {
    st_dump_abc130_mask_registers();
  }

  if(include_hcc) {
    st_dump_hcc_chip_registers();
  }
}

void st_dump_abc130_mask_registers() {
  for(int m = 0; m < 8; m++) {
    int regId = m+0x10;
    e->ReadChipRegister(regId);
    printf("Mask register %d\n", m);

    unsigned char chipAddress;
    unsigned char regAddress;
    unsigned int regValue;
    int c = 0;
    int n = 0;
    while(1) {
      int retVal = st_decode_abc130_chip_register(n, 0, c++, &chipAddress, &regAddress, &regValue);
      if(retVal) break;
      printf(" chip %d  addr %d  value %08x\n", chipAddress, regAddress, regValue);
    }
    c = 0;
    while(1) {
      int retVal = st_decode_abc130_chip_register(n, 1, c++, &chipAddress, &regAddress, &regValue);
      if(retVal) break;
      printf(" chip %d  addr %d  value %08x\n", chipAddress, regAddress, regValue);
    }
  }
}

void st_dump_abcn_chip_registers() {
  int regIds[] = {0, 2, 4, 6, 8, 10, 12, 14, 22, 26, 28, 29, 30, -1};
  int basics[] = {1, 0, 0, 1, 1,  1,  1,  0,  0,  0,  1,  1,  1,  0};
  std::string regNames[] = {"Config", "Trim DAC", "Mask", "Config2", "Cal delay", "Latency",
                            "Threshold", "Fuse (id)", "Status1", "Status2", "Bias 1", "Bias 2", "Bias 3", ""};

  for(int regIndex = 0; regIds[regIndex] != -1; regIndex ++ ) {
    int regId = regIds[regIndex];
    int basic = basics[regIndex];

    e->ReadChipRegister(regId);

    printf("Register %d: %s\n", regId, regNames[regIndex].c_str());

    unsigned char address;
    unsigned int regValue;
    for (int n=0; n<e->GetMaxModules(); n++){
      if(e->m[n]->present) {
        
        printf("Module %2d\nAddress actual expected\n",n);
        int c=0;
        while(1) {
          address = regValue = 0;
          int retVal = st_decode_abcn_chip_register(n, 0, c++, &address, &regValue);
          if(retVal) break;
          if(basic){
            int expValue = e->RetrieveChipBasic(n,address,regId);
            printf("    %03d  %04x  %04x\n", address, regValue, expValue);
          }else{
            printf("    %03d  %04x  ????\n", address, regValue);
          }
        }
        
        c=0;
        while(1) {
          address = regValue = 0;
          int retVal = st_decode_abcn_chip_register(n, 1, c++, &address, &regValue);
          if(retVal) break;
          if(basic){
            int expValue = e->RetrieveChipBasic(n,address,regId);
            printf("    %03d  %04x  %04x\n", address, regValue, expValue);
          }else{
            printf("    %03d  %04x  ????\n", address, regValue);
          }
        }
        printf("\n");
      }
    }
  }
}

bool st_is_abcn() {
  for (int n=0; n<e->GetMaxModules(); n++){
    if(!e->m[n]->present) continue;

    int chipset = e->RetrieveVariable(n, 200);
    if(chipset < 7) {
      return true;
    }
  }

  return false;
}

void st_read_fuse_ids() {
  if(!st_is_abcn()) {
    printf("Can't do fuse id read on ABC130\n");
    return;
  }

  //e->ReadChipRegister(14);
  // Doesn't work the first time...
  e->ReadChipRegister(14);
  unsigned char address;
  unsigned int regValue;
  for (int n=0; n<e->GetMaxModules(); n++){
    if(e->m[n]->present){  // only read present modules
      if(!e->m[n]->chip_info0) {
        printf("TModule forgot to create chip_info0\n");
	    e->m[n]->chip_info0 = new TObjArray;
	  }
	  if(!e->m[n]->chip_info1) {
        printf("TModule forgot to create chip_info1\n");
	    e->m[n]->chip_info1 = new TObjArray;
	  }
      // Also check link0 and link1 in TModule
      int c=0;
      while(1) { // c < ST_CHIPS_PER_LINK
        address = 0; regValue = 0;
        int retVal = st_decode_abcn_chip_register(n, 0, c, &address, &regValue);
        if(retVal) break;
        printf("%d, 0, %d, %d, %x\n", n, c, address, regValue);

        // e->m[n]->chip_info[c].address = address;
        TChip* chip = new TChip;
		chip->address = address;
        chip->fuseId = regValue;
		e->m[n]->chip_info0->AddAtAndExpand(chip, c);
		c++;
      }
      // Make sure no extras
      TObjArray *a1 = e->m[n]->chip_info0;
      printf("C = %d Last = %d\n", c, a1->GetLast());
      for(int i=a1->GetLast() + 1; i>c; i--) {
        // delete a->At(i);
        a1->RemoveAt(i);
      }

      c=0;
      while(1) { // c < ST_CHIPS_PER_LINK
        address = 0; regValue = 0;
        int retVal = st_decode_abcn_chip_register(n, 1, c, &address, &regValue);
        if(retVal) break;
        printf("%d, 1, %d, %d, %x\n", n, c, address, regValue);

        TChip* chip = new TChip;
        chip->address = address;
        chip->fuseId = regValue;

		e->m[n]->chip_info1->AddAtAndExpand(chip, c);
		c++;
	  }
      // Make sure no extras
      TObjArray *a2 = e->m[n]->chip_info1;
      printf("C = %d Last = %d\n", c, a2->GetLast());
      for(int i=a2->GetLast() + 1; i>c; i--) {
        // delete a->At(i);
        a2->RemoveAt(i);
      }
    }
  }
  //printf("Sending HardReset\n");
  //e->HardReset();
  // Put back in data taking mode
  e->ExecuteConfigs(); // PWP 03.12.2008
}

void st_dump_hsio_registers(const char *fname) {
  if(fname == 0) {
    st_dump_hsio_registers_to_stream(std::cout);
	return;
  }

  std::ofstream os(fname, std::ios::app);

  st_dump_hsio_registers_to_stream(os);
  os.close();
}

void st_dump_hsio_registers_to_stream(std::ostream &os) {
  std::string regNames[] = {
        "Input Enables", "Output Enables", "Internal Enables", "Inputs invert",  //0-3
        "Signal Idles", "Sequencer Ctl", "Spy Signals Control", "Length 0", //4-7
        "Length 1", "IDelay", "Post Trig busylen", "OPhase RST/L1R3/L0COM", //8-11
        "[SimDataGen 1 Conf Lo]", "[SimDataGen 1 Conf Hi]", "[SimDataGen Rnd Seeds]", "Trigger window mask", //12-15
        "ABC COM Control", "Busy Delta", "Driver config", "General Control 1", //16-19
        "Display Reg Select (HSIO)", "Debug LEMO Stream Select (HSIO)", "Output Signal Type", "General Control [0]", //20-23
        "TrigBurst Num Trigs.", "TrigBurst Num Bursts.", "TrigBurst Min Period", "TrigBurst Max Period", //24-27
        "TrigBurst Inter Burst", "Trigger Delay", "[BCO Duty Cycle]", "TLU Control", //28-31
        // More recently, more register space, mostly unused at present
        "Readout FIFO Timeouts", "PMOD Control", "OPhase DX", "", //32-35
        "DataGen0 Conf", "DataGen1 Conf", "DataGen2 Conf", "DataGen3 Conf", //36-39
        "", "", "", "", //40-43
        "DuplicateTrig1 Delay", "DuplicateTrig2 Delay", "", "", //44-47
        "", "", "", "", //48-51
        "", "", "", "", //52-55
        "", "", "", "", //56-59
        "", "", "", "", //60-63
    };

  // Readback register block
  uint16_t data[] = {1};
  uint16_t opcode = 0x15;
  uint16_t recv_data[100];
  uint16_t length = e->HsioSendReceiveOpcode(opcode, 0x1234, 0, data, 100, recv_data);

  if(length > 0xf000) {
    os << "No data from HSIO\n";
    return;
  }

  os << "Received register data\n";

  os << "Length: " << length << "\n";
  if(!(length == 32 || length == 64)) {
    os << " *** Unexpected length\n";
  }

  os << "Registers: ";
  for(int i=0; i<length; i++) {
    if((i%8) == 0) os << "\n";
    uint16_t val = recv_data[i];
    val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
    os << " ";
    print_hex(os, 4, val);
  }
  os << "\n";

  os << "Known registers:\n";
  for(int i=0; i<length; i++) {
    if(regNames[i].size() > 0) {
      uint16_t val = recv_data[i];
      val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
      os << Form("%2d(0x%02x) %-26s %04x %5d\n", i, i, regNames[i].c_str(), val, val);
    }
  }
  os << "\n";
}

void st_dump_hsio_status(const char *fname) {
  if(fname == 0) {
    st_dump_hsio_status_to_stream(std::cout);
    return;
  }

  std::ofstream os(fname, std::ios::app);

  st_dump_hsio_status_to_stream(os);
  os.close();
}

void st_dump_hsio_status_to_stream(std::ostream &os) {
  std::string regNames[] = {
    "Hardware ID (0x0C02)", "Sanity (0xA510)", "Firmware Version", "Total Enabled Modules", //0-3
    "TrigBurst Trigger Count", "TrigBurst Burst Count", "TrigBurst Status", "BCID last L1A",//4-7
    "L1ID lo", "L1ID hi", "[SFP0 net lo]", "[SFP1 net hi]", //8-11
    "[SFP1 net lo]", "[SFP1 net hi]", "[Marvel net]", "General Stats", //12-15
    "Top Raw En", "Top Histos En", "Bottom Raw En", "Bottom Histos En", //16-19
    "IDC Histos/Raw En", "Output signals hold reg", "Firmware timestamp Lo", "Firmware timestamp Hi", //20-23
    "SQ address", "SK address", "Last L0ID", "Last TLU TRID", //24-27
    "Ext Trig 0 Count lo", "Ext Trig 0 Count hi", "Ext Trig 1 Count lo", "Ext Trig 1 Count hi", //28-31
    "Firmware Type", "", "[IDELAY Prog Stat]", "", //32-35
    "", "", "", "", //36-39
    "BCR Count lo", "BCR Count hi", "ECR Count lo", "ECR Count hi", //40-43
    "", "", "Busy Assert Count lo", "Busy Assert Count hi", //44-47
    "Busy Duration (15:0)", "Busy Duration (31:16)", "Busy Duration (47:32)", "Busy Duration (63:48)", //48-51
    "", "", "", "", //52-55
    "", "", "", "", //56-59
    "", "", "", "", //60-63
  };

  // Readback status block
  uint16_t data[] = {1};
  uint16_t opcode = 0x19;
  uint16_t recv_data[100];
  uint16_t length = e->HsioSendReceiveOpcode(opcode, 0x1234, 0, data, 100, recv_data);

  if(length > 0xf000) {
    os << "No data from HSIO\n";
    return;
  }

  os << "Received status data\n";

  os << "Length: " << length << "\n";
  if(!(length == 32 || length == 64)) {
    os << " *** Unexpected length\n";
  }

  os << "Registers: ";
  for(int i=0; i<length; i++) {
    if((i%8) == 0) os << "\n";
    uint16_t val = recv_data[i];
    val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
    os << Form(" %04x", val);
  }
  os << "\n";

  os << "Known status registers:\n";
  for(int i=0; i<length; i++) {
    if(regNames[i].size() > 0) {
      uint16_t val = recv_data[i];
      val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
      os << Form("%2d(0x%02x) %-26s %04x %5d\n", i, i, regNames[i].c_str(), val, val);
    }
  }

  if(length > 32) {
    os << "Firmware type: ";
    uint16_t val = recv_data[32];
    val = ((val & 0xff) << 8) | ((val & 0xff00) >> 8);
    switch((val>>8) & 0xff) {
    case 0: os << "ABC130 via IDC"; break;
    case 1: os << "ABCN"; break;
    case 2: os << "ABC130 via Driver"; break;
    case 3: os << "HVCMOS strip v1"; break;
    case 4: os << "Panel"; break;
    case 5: os << "CHESS1"; break;
    }
    switch(val & 0xff) {
    case 1: os << " with TLU"; break;
    case 2: os << " with TDC"; break;
    }
    os << "\n";
  }
  os << "\n";
}

void st_diagnostics_dump() {
  TDatime *dt = new TDatime;
  int date = dt->GetDate();
  delete dt;

  // NB This copies out of Form's temporary buffer
  std::string fname(Form("%sst_diagnostics_%d.txt", getResultsDirectory(), date));

  std::cout << "Writing diagnostics to file " << fname << "\n";

  {
    std::ofstream os(fname.c_str(), std::ios::app);

    os << "==================================================\n";
    os << "================ Diagnostics dump ================\n";
    os << "==================================================\n";

    os.close();
  }

  // Being paranoid and not passing ostream across object boundary

  e->ShowSysmapToFile(fname.c_str());

  {
    std::ofstream os(fname.c_str(), std::ios::app);
    os << "==================\n";
    os << "= Module configs =\n";
    os << "==================\n";
    os.close();
  }

  for(int m=0; m<e->GetLastPresentId()+1; m++) {
    if(!e->m[m]->present) continue;
    e->ShowModuleToFile(m, fname.c_str());
  }

  {
    std::ofstream os(fname.c_str(), std::ios::app);
    os << "===============\n";
    os << "= System info =\n";
    os << "===============\n";
    os.close();
  }

  e->PrintStatusToFile(fname.c_str());

  {
    std::ofstream os(fname.c_str(), std::ios::app);
    os << "============\n";
    os << "= DAQ info =\n";
    os << "============\n";
    os.close();
  }

  e->DAQStatusToFile(fname.c_str());
  st_dump_hsio_registers(fname.c_str());
  st_dump_hsio_status(fname.c_str());

  {
    std::ofstream os(fname.c_str(), std::ios::app);
    os << "===================\n";
    os << "= End diagnostics =\n";
    os << "===================\n";
    os.close();
  }
}

void st_scan(short burst_type, short scan_type){
  if(e == 0) {
    printf("st_scan called before TST created\n");
    return;
  }

  // The Basic Scan Routine!!
  // Revised PWP 09.05.01 to allow:
  //   Adjustment of burst_ntrigs during NO scans
  //   Autostop

  int stop_loop = 0;
  int n_to_continue=0;
  short n, fitf;

  e->abort=0;

  e->ConfigureScanDefaults(scan_type);

  e->burst_type = burst_type;
  e->InitScan();

  stop_loop = 0;
  
  // e->HardReset();
  e->Sleep(200);
  e->ExecuteConfigs();
  e->Sleep(200);

  bool showDisplay2 = false;
  if((e->RetrieveVariable(0, 200) < 7)
    && ((burst_type==21)||(burst_type==100)||(burst_type==102)||(burst_type==112))) {
    // Sort of works for ABCN
    showDisplay2 = true;
  } else if((((int)e->RetrieveVariable(0, 200)) == 7)
         && ((burst_type==100))) {
    // Now have cluster size for decoded burst on ABC130
    showDisplay2 = true;
  }

  // How long the last display took
  float display_time = 0;
  // How long the last burst took
  float burst_time = 0;
  // Is there a burst that we haven't displayed (at the end)
  bool burst_not_displayed = false;

  TStopwatch since_display_timer;

  while(e->scan_running){ 
    TStopwatch burst_timer;
    e->ScanPoint();
    burst_time = burst_timer.RealTime();

    printf("ST::st_scan: finished ScanPoint()\n");
    // Try not to slow things down just to display the burst progress
    bool do_display_burst = false;
    float since_display_time = since_display_timer.RealTime();
    if(since_display_time > 4*display_time) do_display_burst = true;
    else if(burst_time > 2*display_time) do_display_burst = true;

    if(do_display_burst) {
      TStopwatch display_timer;
      st_display_burst();
      if(showDisplay2) {
        st_display_burst2();
      }
      display_time = display_timer.RealTime();
      burst_not_displayed = false;
      printf("Burst timings display %.3fs last burst %.3fs (%.3fs since last display)\n", display_time, burst_time, since_display_time);
      since_display_timer.Start();
    } else {
      printf("Skip display burst %.3fs vs %.3fs for burst (%.3fs since last display)\n", display_time, burst_time, since_display_time);
      burst_not_displayed = true;
      since_display_timer.Continue();
    }

    // Adjustment of burst_ntrigs for noise occupancy scans
    if((e->do_vary_ntrigs)&&(  (burst_type== 12)    // VBURST
                             ||(burst_type== 17)    // NSEBURST2
                             ||(burst_type==100)    // DBURST
                             ||(burst_type==102))){ // DVBURST
      if(e->tsent>(4*e->burst_ntrigs)){
        e->burst_ntrigs*=2;
      }
    }

    // AutoStop facility
    if(e->do_autostop){
      n_to_continue=0;
      for(n=0;n<e->GetMaxModules();n++){
        if(e->m[n]->present){
          if(e->m[n]->OKtoStop(e->scan.scan_type)){
            printf("  Module %d OK to stop\n",n);
          }else{
            printf("  Module %d needs to continue\n",n);
            n_to_continue++;
          }
        }
      }
      if(n_to_continue>0){
        printf("    Total of %d modules need to continue\n",n_to_continue);
      } else {
        printf("    STOPPING due to AUTOSTOP!!\n");
        e->scan_running=0;
      }
    }

    stop_loop=st_do_stop();
    if(stop_loop==1) e->scan_running=0; // abort if stopped
  }

  if(burst_not_displayed) {
    st_display_burst();
    if(showDisplay2) {
      st_display_burst2();
    }
  }

  e->EndScan();
  st_display_scan();

  if(e->do_fits){
    switch(e->scan.scan_type){
      case 1:   // ST_VTHR
      case 3:   // ST_QTHR
      case 41:  // ST_STROBE_DELAY
      case 1002: // ABCD_VTHP
      case 1003: // ABCD_VTHN
        fitf = 1;
        break;
      case 6:   // ST_TRIM
      case 2:   // ST_VCAL
      case 42:  // ST_QCAL
        fitf = 2;
        break;
      default:
        fitf =-1;
        break;
    }
      
    for (n=0; n<e->GetMaxModules(); n++){
      if(e->m[n]->present){  // only fit scan for present modules
        printf("Fitting Scan for module %d\n",n);
        if((e->RetrieveVariable(n, 200) < 7)
            && !((e->do_cal_loop > 0) || (e->burst_trtype == 0))) {
          // BJG: If cal_loop is not on and trigger is not single L1A
          //  then FitScan is done for only one cal line
          //  This isn't useful for ABC130, when we can inject on all channels
          printf("*** NB, I'm fitting only some of the channels ****\n");
          e->m[n]->FitScan(fitf,0);
        } else {
          e->m[n]->FitScan(fitf,-1);
        }
        e->m[n]->PrintScan();
      }
    }
    st_display_scanfits();
  }

//  Next section commented out, PWP 14.03.02
//  if(e->do_action_DCS){
//    st_display_scan_dcs();
//  }

  if(e->do_read_post_scan_config) {
    st_read_fuse_ids();

    if(e->HccPresent()) {
      // Read AM and store in TTree
      st_hcc_am_read(true, false);
    }
  }

  st_save_scan();
}

void st_display_scan() {

  if(!scanDisp) {
    printf("Can't find scan window to do update!\n");
    return;
  }

  scanDisp->UpdateScan();
  scanDisp->UpdateDCS();
  gSystem->ProcessEvents();
}

void st_display_old_scan(int run, int scan) {
  if(run == -1) run = e->runnum;
  if(scan == -1) scan = e->scannum;
  char displayName[30];
  sprintf(displayName, "ScanData_%d_%d", run, scan);
  STGUIDisplayCommon *oldScanDisp = STGUI::NewScan(displayName);
  char fname[200];
  BuildScanFileName(fname, run, scan);
  oldScanDisp->UpdateFromFile(fname);
  gSystem->ProcessEvents();
}

void st_display_scanfits(){
  if(scanDisp) {
    scanDisp->UpdateScanFits();
  }
  gSystem->ProcessEvents();
}

void st_styles(){ 
  gStyle->SetHistFillColor(46);

  gStyle->SetPadLeftMargin(0.1);
  gStyle->SetPadRightMargin(0.1);
  gStyle->SetPadTopMargin(0.1);
  gStyle->SetPadBottomMargin(0.1);

  gStyle->SetHistTopMargin(0.1);

  gStyle->SetScreenFactor(1);

  gStyle->SetPadBorderSize(0);
  gStyle->SetPadBorderMode(0);
  gStyle->SetFrameBorderMode(1);
  gStyle->SetTitleW(0.6);
  gStyle->SetTitleH(0.08);
  gStyle->SetOptStat(11);
  gStyle->SetStatH(0.3);

  gStyle->SetCanvasColor(kWhite);
  gStyle->SetPadColor(kWhite);
  gStyle->SetStatColor(kWhite);

  gStyle->SetLabelSize(0.06,"x");
  gStyle->SetLabelSize(0.06,"y");
}

void st_burst_PaveText(){
  TPaveText* t1;

  gPad->Clear();
  t1= e->GetBurstPaveText();
  t1->Draw();
  gPad->Modified();
}

void st_scan_PaveText(){
  TPaveText* t1;

  gPad->Clear();
  t1= e->GetScanPaveText();
  t1->Draw();

  gPad->Modified();
}

void st_display_burst() {
  if(!burstDisp) {
    printf("Failed to update burst, lost track of window\n");
    return;
  }
  burstDisp->UpdateBurst();
  burstDisp->UpdateDCS();
  gSystem->ProcessEvents();
}


void st_display_burst2() {
  if(gROOT->IsBatch()) {
    printf("Batch mode, skip display extended burst\n");
	return;
  }

  char s[128],t[128],h_name[64],mname[32];
  int col = 17;
  int n,npan,ipan,nmodppan;

  TCanvas *pburst2[4];
  TPad *pburst2_info[4], *pburst2_hist[4];

  nmodppan = e->num_panelmods;
  npan = (e->num_showmods-1)/nmodppan+1;
  if(npan > 4) npan = 4;

  for(ipan=0;ipan<npan;ipan++){
    sprintf(t,"pburstII%d",ipan);
    pburst2[ipan] = (TCanvas*) gROOT->FindObject(t);

    if(pburst2[ipan]==0){
      st_styles();
      if(npan == 1) 
        sprintf(s,"Burst2 Display  Run %d", e->runnum);
      else 
        sprintf(s,"Burst2 Display  Run %d Modules %d-%d", e->runnum,ipan*nmodppan,(ipan+1)*nmodppan-1);
      pburst2[ipan] = new TCanvas(t,s,1100,750);
      pburst2[ipan]->SetFillColor(18);
    }
    pburst2[ipan]->cd();

    sprintf(t,"pburstII_info%d",ipan);
    pburst2_info[ipan] = (TPad*) gROOT->FindObject(t);
    if(pburst2_info[ipan]==0){
      pburst2[ipan]->cd();
      pburst2_info[ipan] = new TPad(t,"Burst Info Pad",0.0,0.88,1.0,1.0,col);
      pburst2_info[ipan]->Draw();
      pburst2_info[ipan]->cd();
      pburst2_info[ipan]->Divide(2,1,0,0);
    }
    
    sprintf(t,"pburstII_hist%d",ipan);
    pburst2_hist[ipan] = (TPad*) gROOT->FindObject(t);
    if(pburst2_hist[ipan]==0){
      pburst2[ipan]->cd();
      pburst2_hist[ipan] = new TPad(t,"Burst Histogram Pad", 0.0,0.0,1.0,0.88,col);
      pburst2_hist[ipan]->Draw();
      pburst2_hist[ipan]->cd();
      pburst2_hist[ipan]->Divide(5,nmodppan,0,0);
    }
    
    pburst2_info[ipan]->cd(1);
    gPad->Clear();
    e->GetBurstPaveText()->Draw();
    //    st_burst_PaveText();
    
    pburst2_info[ipan]->cd(2);
    e->h_merr->Draw();
    
    for (n=ipan*nmodppan; n<e->num_showmods && n<(ipan+1)*nmodppan; n++){
      pburst2_hist[ipan]->cd(((n-ipan*nmodppan)*5)+1);
      e->m[n]->h_cluster0->Draw();
      pburst2_hist[ipan]->cd(((n-ipan*nmodppan)*5)+2);
      e->m[n]->h_cluster1->Draw();
      
      e->GetModuleName(n,mname);
      
      sprintf(h_name,"h_hit%d_0",n);
      TH1F *h_hit0 = (TH1F*) gDirectory->GetList()->FindObject(h_name);
      if(h_hit0) h_hit0->Delete();  //avoid memory leaks
      sprintf(s,"Module %d %s  Stream 0 Hits",n,mname);
      e->m[n]->h_corr->ProjectionX(h_name,0,128);
      h_hit0 = (TH1F*) gDirectory->GetList()->FindObject(h_name);
      pburst2_hist[ipan]->cd(((n-ipan*nmodppan)*5)+3);
      if(h_hit0) {
        h_hit0->SetTitle(s);
        h_hit0->SetXTitle("Hits Stream 0");
        h_hit0->SetYTitle("n(Events)");
        h_hit0->Draw();
      }

      sprintf(h_name,"h_hit%d_1",n);
      TH1F *h_hit1 = (TH1F*) gDirectory->GetList()->FindObject(h_name);
      if(h_hit1) h_hit1->Delete();  //avoid memory leaks
      sprintf(s,"Module %d %s  Stream 1 Hits",n,mname);
      e->m[n]->h_corr->ProjectionY(h_name,0,128);
      h_hit1 = (TH1F*) gDirectory->GetList()->FindObject(h_name);
      if(h_hit1) {
        pburst2_hist[ipan]->cd(((n-ipan*nmodppan)*5)+4);
        h_hit1->SetTitle(s);
        h_hit1->SetXTitle("Hits Stream 1");
        h_hit1->SetYTitle("n(Events)");
        h_hit1->Draw();
      }
      
      pburst2_hist[ipan]->cd(((n-ipan*nmodppan)*5)+5);
      e->m[n]->h_corr->Draw();
    }

    pburst2[ipan]->Modified();
    pburst2[ipan]->Update();
  }

  gSystem->ProcessEvents();
}

void st_display_dcs() {
  if(gROOT->IsBatch()) {
    printf("Batch mode, skip display DCS info\n");
	return;
  }

  char s[128],t[128];
  int col = 17;
  int n, npan, ipan, nmodppan;

  TCanvas *pburst[4];
  TPad *pburst_text[4];

  nmodppan = e->num_panelmods;
  npan = (e->num_showmods-1)/nmodppan+1;
  if(npan > 4) npan = 4;

//  e->DCSQuery();
  e->DCSAction(); // update information for all present modules
                  // not just for e->num_showmods  PWP 11.02.03

  for(ipan=0;ipan<npan;ipan++){
    sprintf(t,"pburst%d",ipan);
    pburst[ipan] = (TCanvas*) gROOT->FindObject(t);
    if(pburst[ipan]==0){
      st_styles();
      if(npan == 1)
        sprintf(s,"Burst Display  Run %d", e->runnum);
      else 
        sprintf(s,"Burst Display  Run %d Modules %d-%d", e->runnum,ipan*nmodppan,(ipan+1)*nmodppan-1);
      pburst[ipan] = new TCanvas(t,s,1100,750);
      pburst[ipan]->SetFillColor(18);
    }
    pburst[ipan]->cd();

    sprintf(t,"pburst_text%d",ipan);
    pburst_text[ipan] = (TPad*) gROOT->FindObject(t);
    if(pburst_text[ipan]==0){
      pburst[ipan]->cd();
      pburst_text[ipan] = new TPad(t,"Burst Text Pad", 0.9,0.0,1.0,0.88,col);
      pburst_text[ipan]->Draw();
      pburst_text[ipan]->cd();
      pburst_text[ipan]->Divide(1,nmodppan,0,0);
    }

    for (n=ipan*nmodppan; n<e->num_showmods && n<(ipan+1)*nmodppan; n++){
      pburst_text[ipan]->cd(n-ipan*nmodppan+1);
      gPad->Clear();
      e->m[n]->GetDCSBurstText()->Draw();
    }
    pburst[ipan]->Modified();
    pburst[ipan]->Update();
  }

  gSystem->ProcessEvents();
}

void st_save_scan(TFile* out, Int_t accbst) {

  int n;
  char fname[128];
  char mname[32];
  Float_t tmp[6];
  TGraph* gr;

  int write_fits=0;

  // this function should be viewed as purely temporary... famous last words !

  BuildScanFileName(fname,e->runnum,e->scannum);
  if(out==0) out = new TFile(fname,"RECREATE");

  if(out == 0 || !out->IsOpen()) {
    printf("Failed to open data file %s, forgetting results\n", fname);
    return;
  }

  TDatime *dt = new TDatime;
  TObjString *dts = new TObjString(dt->AsString());
  TObjString *pls = new TObjString(getLocationName().c_str());
  TObjString *host= new TObjString(gSystem->Getenv("COMPUTERNAME"));
  if(host == 0) {
    // eg Linux
    host = new TObjString(gSystem->Getenv("HOSTNAME"));
  }
  TObjString *user= new TObjString(gSystem->Getenv("SCTDB_USER"));

  dts->Write("Time");
  pls->Write("Place");
  host->Write("Host");
  user->Write("User");

  for (n=0; n<e->GetMaxModules(); n++){
    if(e->m[n]->scan_fitted){
      write_fits++;
    }
  }

  // write remote environmental info to file
  if(st_get_dcsrem(tmp)){
    sprintf(fname,"%3.1f",tmp[0]);
    TObjString * et1s = new TObjString(fname);
    et1s->Write("ET1");
    delete et1s;
    
    sprintf(fname,"%3.1f",tmp[1]);
    TObjString * et2s = new TObjString(fname);
    et2s->Write("ET2");
    delete et2s;
    
    sprintf(fname,"%3.1f",tmp[2]);
    TObjString * et3s = new TObjString(fname);
    et3s->Write("ET3");
    delete et3s;
    
    sprintf(fname,"%3.1f",tmp[3]);
    TObjString * hums = new TObjString(fname);
    hums->Write("HUM");
    delete hums;
    
    sprintf(fname,"%3.1f",tmp[4]);
    TObjString * pips = new TObjString(fname);
    pips->Write("PIPE");
    delete pips;
  }

  for (n=0; n<e->GetMaxModules(); n++){
    // Can't do this because cycle number represents the module number for configuration feedback!
  //  if(e->m[n]->present){  // only write histograms for present modules

      e->GetModuleName(n,mname);
      TObjString * mns = new TObjString(mname);
      mns->Write("ModuleName");
      delete mns;

      e->GetDeviceType(n,fname);
      TObjString * dut = new TObjString(fname);
      dut->Write("DUT");
      delete dut;

      if(e->m[n]->present) {
        e->m[n]->DCS->WriteTree();
      }

      if(st_is_abcn()) {
        printf("Writing chip info for %d\n", n);

        if(e->m[n]->chip_info0) {
          sprintf(fname,"chip_info0_%d",n);
          e->m[n]->chip_info0->Write(fname);
          printf("Written chip info for link 0\n");
        } else {
          printf("Chip info for link 0 not set\n");
        }
        if(e->m[n]->chip_info1) {
          sprintf(fname,"chip_info1_%d",n);
          e->m[n]->chip_info1->Write(fname);
          printf("Written chip info for link 1\n");
        } else {
          printf("Chip info for link 1 not set\n");
        }
      }

      // sprintf formats in this section revised PWP 28.03.02
      sprintf(fname,"%2.1f",e->m[n]->DCS->T1);
      TObjString * t1s = new TObjString(fname);
      t1s->Write("T0");
      delete t1s;

      sprintf(fname,"%2.1f",e->m[n]->DCS->T2);
      TObjString * t2s = new TObjString(fname);
      t2s->Write("T1");
      delete t2s;

      sprintf(fname,"%3.1f",e->m[n]->DCS->vdet);
      TObjString * vdets = new TObjString(fname);
      vdets->Write("VDET");
      delete vdets;

      sprintf(fname,"%1.2f",e->m[n]->DCS->idet);
      TObjString * idets = new TObjString(fname);
      idets->Write("IDET");
      delete idets;

      sprintf(fname,"%1.2f",e->m[n]->DCS->vcc);
      TObjString * vccs = new TObjString(fname);
      vccs->Write("VCC");
      delete vccs;

      sprintf(fname,"%3.0f",e->m[n]->DCS->icc);
      TObjString * iccs = new TObjString(fname);
      iccs->Write("ICC");
      delete iccs;

      sprintf(fname,"%1.2f",e->m[n]->DCS->vdd);
      TObjString * vdds = new TObjString(fname);
      vdds->Write("VDD");
      delete vdds;

      sprintf(fname,"%3.0f",e->m[n]->DCS->idd);
      TObjString * idds = new TObjString(fname);
      idds->Write("IDD");
      delete idds;

      // e->m[n]->DCS->Write();  //stream the DCS object

      if(accbst && e->m[n]->present){
        e->m[n]->h_accbst0->Write();
        e->m[n]->h_accbst1->Write();
      }
      else{
        e->m[n]->h_scan0->Write("h_scan0");
        e->m[n]->h_scan1->Write("h_scan1");

        if(e->nSCTLV>0){
          gr = e->m[n]->GetIddGraph();
          gr->Write("Idd");
          gr = e->m[n]->GetIccGraph();
          gr->Write("Icc");
        }
        if(e->nSCTHV>0 || e->m[n]->DCS->hv_source==2){
          gr = e->m[n]->GetIdetGraph();
          gr->Write("Idet");
        }

        if(write_fits){
          e->m[n]->h_mean0->Write("h_mean0");
          e->m[n]->h_mean1->Write("h_mean1");
          e->m[n]->h_sigma0->Write("h_sigma0");
          e->m[n]->h_sigma1->Write("h_sigma1");
          e->m[n]->h_code0->Write("h_code0");
          e->m[n]->h_code1->Write("h_code1");
          e->m[n]->h_chisq0->Write("h_chisq0");
          e->m[n]->h_chisq1->Write("h_chisq1");
          e->m[n]->h_prob0->Write("h_prob0");
          e->m[n]->h_prob1->Write("h_prob1");
          e->m[n]->h_fom0->Write("h_fom0");
          e->m[n]->h_fom1->Write("h_fom1");
        }
      }

      // TODO: Write the next three hists at each EOB
      // This code will write the hists to file only
      // The following histograms are only generated by
      // burst types which use SOFTWARE histogramming

      if((e->burst_type==21) ||(e->burst_type==30)||   // decoded eburst or decoded sburst
         (e->burst_type==100)||(e->burst_type==102)){  // decoded burst  or decoded vburst 

        e->m[n]->h_hitsLink0->Write("h_hitsPerLink0");
        e->m[n]->h_hitsLink1->Write("h_hitsPerLink1");

        e->m[n]->h_cluster0->Write("h_cluster0");
        e->m[n]->h_cluster1->Write("h_cluster1");
        e->m[n]->h_corr->Write("h_corr");

        if(e->burst.fill_evtree || e->burst.fill_tree_counter) {
          if(e->m[n]->evtree!=0){
            sprintf(fname,"EvTree_m%d",n);
            e->m[n]->evtree->Write(fname);
          }
        }
      }
  //  } 
  }

  e->h_scan_tsent->Write();
  for (n=0; n<e->nDAQCards; n++){
    e->h_scan_evcnt[n]->Write("h_scan_evcnt");
    e->h_scan_ercnt[n]->Write("h_scan_ercnt");
    e->h_scan_tocnt[n]->Write("h_scan_tocnt");
    e->h_scan_xecnt[n]->Write("h_scan_xecnt");
  }

  e->WriteScanInfo();

  // Write to disk before closing
  out->Write();
  
  out->Close();


  sprintf(fname, "%sstlog%d.txt",getDataDirectory(),e->runnum);
  
  FILE *logfile;
  logfile = fopen(fname,"a+");

  fprintf(logfile,"runnum %u\n",e->runnum);
  fprintf(logfile,"scannum %u\n",e->scannum);
  fprintf(logfile,"date %u\n",dt->GetDate());
  fprintf(logfile,"time %u\n",dt->GetTime());
  fprintf(logfile,"scan_type %u\n",e->scan.scan_type);
  fprintf(logfile,"scan_start %6.2f\n",e->scan.scan_start);
  fprintf(logfile,"scan_stop %6.2f\n",e->scan.scan_stop);
  fprintf(logfile,"scan_step %6.2f\n",e->scan.scan_step);
  fprintf(logfile,"run_tsent %u\n",(unsigned int)e->run_tsent);
  fprintf(logfile,"run_evcnt %u\n",(unsigned int)e->run_evcnt);
  fprintf(logfile,"run_ercnt %u\n",(unsigned int)e->run_ercnt);
  fprintf(logfile,"run_tocnt %u\n",(unsigned int)e->run_tocnt);
 
  fclose(logfile);

  delete dt; // PWP 20.12.01
  delete dts;
  delete pls;
  delete host;
  delete user;
}

void st_dcs_query() {
  int moi;

  printf("Show DCS for module (int) ?");
  scanf("%d",&moi);

  if((moi>=0)&&(moi<e->GetMaxModules())){
    e->m[moi]->DCS->Print();
  } else {
   printf("Sorry, module %d out of range\n",moi);
  }
}

void st_dcs_log() {
  int moi;
  float tmp[6];
  
  printf("\nModule-specific DCS-Information:\n\n");
  printf("Module\t\tVcc\tIcc\tVdd\tIdd\tVdet\tIdet\tThyb1\tThyb2\n");

  for(moi=0;moi<e->GetMaxModules();moi++){ // was e->num_showmods PWP 11.02.03
    if(e->m[moi]->present) {
      e->m[moi]->DCS->Query();
      printf("%s\t%.2f\t%.0f\t%.2f\t%.0f\t%.1f\t%.2f\t%.2f\t%.2f\n",
             e->m[moi]->name.c_str(),
             e->m[moi]->DCS->vcc,e->m[moi]->DCS->icc,
             e->m[moi]->DCS->vdd,e->m[moi]->DCS->idd,
             e->m[moi]->DCS->vdet,e->m[moi]->DCS->idet,
             e->m[moi]->DCS->T1,e->m[moi]->DCS->T2);
    }
  }

  if(st_get_dcsrem(tmp)){
    printf("\nEnvironmental DCS-Information:\n\n");
    printf("Tamb1\tTamb2\tTamb3\tHum\tTpipe\n");
    printf("%.1f\t%.1f\t%.1f\t%.1f\t%.1f\n\n",
           tmp[0],tmp[1],tmp[2],tmp[3],tmp[4]);
  }

}

Int_t st_get_dcsrem(Float_t *info){

  if("B186 Barrel System Test" == getLocationName()) {
    info[0] = e->DCSMessg("temperature 0");
    info[1] = e->DCSMessg("temperature 1");
    info[2] = e->DCSMessg("temperature 2");
    info[3] = e->DCSMessg("humidity 0");
    info[4] = e->DCSMessg("chiller 0");
    return 1;
  }

  if("B186 Forward System Test" == getLocationName()) {
    info[0] = e->DCSMessg("ftemperature 0");
    info[1] = e->DCSMessg("ftemperature 1");
    info[2] = e->DCSMessg("ftemperature 2");
    info[3] = e->DCSMessg("humidity 1");
    info[4] = e->DCSMessg("chiller 2");
    return 1;
  }

  return 0;
}

#if 0
void st_show_sc() {
  int moi,loi;

  printf("Show s curves for module (int) ?");
  scanf("%d",&moi);
  
  printf("link ?");
  scanf("%d",&loi);
  
  gROOT->ProcessLine(".L stan_scurves.cpp");
  stan_scurves(moi,loi);
}

void st_kwikplot() {
  int moi;

  printf("Display last scan for module (int) ?");
  scanf("%d",&moi);
  
  gROOT->ProcessLine(".L KwikPlot.cpp");
  KwikPlot(moi);
}

void st_fitscan() {
  int moi,loi;

  printf("Fit last scan for module (int) ?");
  scanf("%d",&moi);
  
  printf("link ?");
  scanf("%d",&loi);
  
  gROOT->ProcessLine(".L stan_fitscan.cpp");
  stan_fitscan(moi,loi);
}
#endif

int st_do_stop(){
  // If STOP was signalled, as opposed to ABORT, clear signal
  if(e->abort==2){
    e->abort=0;
    return 1;  // STOP
  }
  return e->abort;
}

void st_docs(){
  // use this line to browse the local documentation
  // sct_path was removed because it was hard-coded and should be dynamic

  // and this one to browse the documentation at CERN

  const char *url = Form("%s/www/index.html", gSystem->WorkingDirectory());

  // Allow menu to close before Exec (otherwise X still locked)
  gSystem->ProcessEvents();

  if(strncmp(gSystem->GetName(),"Unix",4)==0) {
    // Linux only has the command thread, so explicitly run in background
    gSystem->Exec(Form("xdg-open %s &", url));
  } else {
    gSystem->Exec(Form("START %s", url));
  }

  const char *other_url = "http://atlas.web.cern.ch/Atlas/GROUPS/INNER_DETECTOR/SCT/testdaq/testdaq.html";
  const char *another_url = "https://twiki.cern.ch/twiki/bin/view/Atlas/StripsUpgradeDAQ";

  printf("Old docs (from SCT days):\n %s\n", other_url);
  printf("Twiki docs (probably more up to date):\n %s\n", another_url);
}

void st_display_scan_counters() {

  short n;
  char s[128];
  int col = 17;

  TCanvas* pcount = (TCanvas*) gROOT->FindObject("pcount");
  if(pcount==0){
    st_styles();
    sprintf(s,"Counter Display  Run %d", e->runnum);
    pcount = new TCanvas("pcount",s,1100,750);
    pcount->SetFillColor(18);
  }
  pcount->cd();
 
  TPad* pcount_info = (TPad*) gROOT->FindObject("pcount_info");
  if(pcount_info==0){
    pcount->cd();
    pcount_info = new TPad("pcount_info","Scan Info Pad",0.0,0.8,0.5,1.0,col);
    pcount_info->Draw();
  }

  TPad* pcount_tsent = (TPad*) gROOT->FindObject("pcount_tsent");
  if(pcount_tsent==0){
    pcount->cd();
    pcount_tsent = new TPad("pcount_tsent","Scan TSent Pad",0.5,0.8,1.0,1.0,col);
    pcount_tsent->Draw();
  }

  TPad* pcount_hist = (TPad*) gROOT->FindObject("pcount_hist");
  if(pcount_hist==0){
    pcount->cd();
    pcount_hist = new TPad("pcount_hist","Scan MuSTARD Counters Pad", 0.0,0.0,1.0,0.8,col);
    pcount_hist->Draw();
    pcount_hist->cd();
    pcount_hist->Divide(4,e->nDAQCards,0,0);
  }

  pcount_info->cd();
  pcount_info->Clear();
  st_scan_PaveText();
  
  pcount_tsent->cd();
  e->h_scan_tsent->Draw();

  for (n=0; n<e->nDAQCards; n++){
    pcount_hist->cd(1+(4*n));
    e->h_scan_evcnt[n]->Draw();
    pcount_hist->cd(2+(4*n));
    e->h_scan_ercnt[n]->Draw();
    pcount_hist->cd(3+(4*n));
    e->h_scan_tocnt[n]->Draw();
    pcount_hist->cd(4+(4*n));
    e->h_scan_xecnt[n]->Draw();
  }

  pcount->Modified(); 
  pcount->Update();

  gSystem->ProcessEvents();
}

void st_display_scan_dcs() {

  short n;
  char s[128];
  int col = 17;

  TCanvas* dcs = (TCanvas*) gROOT->FindObject("dcs");
  if(dcs==0){
    st_styles();
    sprintf(s,"DCS Display  Run %d", e->runnum);
    TCanvas* dcs = new TCanvas("dcs",s,1100,750);
    dcs->SetFillColor(18);
  }
  dcs->cd();
 
  TPad* dcs_info = (TPad*) gROOT->FindObject("dcs_info");
  if(dcs_info==0){
    dcs->cd();
    dcs_info = new TPad("dcs_info","Scan Info Pad",0.0,0.8,0.5,1.0,col);
    dcs_info->Draw();
  }

  TPad* dcs_tsent = (TPad*) gROOT->FindObject("dcs_tsent");
  if(dcs_tsent==0){
    dcs->cd();
    dcs_tsent = new TPad("dcs_tsent","Scan TSent Pad",0.5,0.8,1.0,1.0,col);
    dcs_tsent->Draw();
  }

  TPad* dcs_hist = (TPad*) gROOT->FindObject("dcs_hist");
  if(dcs_hist==0){
    dcs->cd();
    dcs_hist = new TPad("dcs_hist","Scan MuSTARD Counters Pad", 0.0,0.0,1.0,0.8,col);
    dcs_hist->Draw();
    dcs_hist->cd();
    dcs_hist->Divide(e->num_showmods,3,0,0);
  }

  dcs_info->cd();
  dcs_info->Clear();
  st_scan_PaveText();

  dcs_tsent->cd();
  e->h_scan_tsent->Draw();

  TGraph* gr_dcs[18][3];

  for (n=0; n<e->num_showmods; n++){

    if(e->m[n]->present){

      gr_dcs[n][0]= 0;
      gr_dcs[n][1]= 0;
      gr_dcs[n][2]= 0;

      if(e->nSCTLV>0){
        gr_dcs[n][0]= e->m[n]->GetIccGraph();
        gr_dcs[n][1]= e->m[n]->GetIddGraph();
      }

      if(e->nSCTHV>0){
        gr_dcs[n][2]= e->m[n]->GetIdetGraph();
      }

      if(gr_dcs[n][0]){   
        dcs_hist->cd(n+1);
        gr_dcs[n][0]->SetMarkerStyle(21);
        gr_dcs[n][0]->Draw("AP");
        gr_dcs[n][0]->GetHistogram()->SetXTitle(e->scan_name);
        gr_dcs[n][0]->GetHistogram()->SetYTitle("Icc (mA)");
        gr_dcs[n][0]->GetHistogram()->Draw();
        gr_dcs[n][0]->Draw("P");
      }

      if(gr_dcs[n][1]){
        dcs_hist->cd((n+1)+e->num_showmods);
        gr_dcs[n][1]->SetMarkerStyle(21);
        gr_dcs[n][1]->Draw("AP");
        gr_dcs[n][1]->GetHistogram()->SetXTitle(e->scan_name);
        gr_dcs[n][1]->GetHistogram()->SetYTitle("Idd (mA)");
        gr_dcs[n][1]->GetHistogram()->Draw();
        gr_dcs[n][1]->Draw("P");
      }

      if(gr_dcs[n][2]){
        dcs_hist->cd((n+1)+(2*e->num_showmods));
        gr_dcs[n][2]->SetMarkerStyle(21);
        gr_dcs[n][2]->Draw("AP");
        gr_dcs[n][2]->GetHistogram()->SetXTitle(e->scan_name);
        gr_dcs[n][2]->GetHistogram()->SetYTitle("Idet (mA)");
        gr_dcs[n][2]->GetHistogram()->Draw();
        gr_dcs[n][2]->Draw("P");
      }

    }   
  }

  dcs->Modified(); 
  dcs->Update();

  gSystem->ProcessEvents();
}

#if 0
// links to production test macros
// added PWP 16.11.00

void st_tm(){
  gROOT->ProcessLine(".L StreamDelay.cpp");
  StreamDelay(0);
}

void st_iv(){
  gROOT->ProcessLine(".L IVCurve.cpp");
  IVCurve();
}

void st_bt(){
  gROOT->ProcessLine(".L BypassTest.cpp");
  BypassTest();
}

void st_abcn_sd(){
  gROOT->ProcessLine(".L ABCNStrobeDelay.cpp");
  ABCNStrobeDelay(1);
}

void st_abcn_tpg(){
  gROOT->ProcessLine(".L ABCNThreePointGain.cpp");
  ABCNThreePointGain();
}

void st_abcn_tr(){
  gROOT->ProcessLine(".L ABCNTrimRange.cpp");
  ABCNTrimRange();
}

void st_ts(){
  Float_t charge;
  Int_t TrimRange;
  printf("TrimScan: Trim with charge: ");
  scanf("%f",&charge);
  printf("and TrimRange: ");
  scanf("%d",&TrimRange);

  gROOT->ProcessLine(".L TrimScan.cpp");
  TrimScan(charge,TrimRange,0,1);
}

void st_abcn_rc(){
  gROOT->ProcessLine(".L ABCNResponseCurve.cpp");
  ABCNResponseCurve();
}

void st_abcn_no(Int_t go_slow, Int_t do_qthr){
  gROOT->ProcessLine(".L ABCNNo.cpp");
  ABCNNo(go_slow,do_qthr);
}

void st_abcn_tw(){
  gROOT->ProcessLine(".L ABCNTimewalk.cpp");
  ABCNTimewalk(1);
}
#endif

void st_nmask(int burst_type, int ntrigs, int trtype, int bins) {
  e->SaveState();
  e->CacheMasks(0);

  e->burst_ntrigs = ntrigs;
  e->burst_trtype = trtype;
  e->do_cal_loop = 0;
  e->do_fits = 0;

  if(trtype == 100) {
    uint16_t nt = 0; // Triggers per "burst" (-1)
    uint16_t nb = ntrigs-1;   // Bursts of triggers (-1)
    uint16_t rmin = 500; // Min randomiser (400ns steps)
    uint16_t rmax = 500; // Max randomiser   "    "
    // Units of 400ns which is roughly 1 packet (at 160)
    //  Max is 64 per chip and 16 chips = 1024
    //  Add a margin for reliability
    uint16_t inter = 3500; // Between bursts "    "

    e->ConfigureVariable(ST_HSIO_REG_BASE + 0x18, nt);
    e->ConfigureVariable(ST_HSIO_REG_BASE + 0x19, nb);
    e->ConfigureVariable(ST_HSIO_REG_BASE + 0x1a, rmin);
    e->ConfigureVariable(ST_HSIO_REG_BASE + 0x1b, rmax);
    e->ConfigureVariable(ST_HSIO_REG_BASE + 0x1c, inter);

    // Tell FW to generate L1 too
    e->ConfigureVariable(ST_HSIO_REG_BASE + 19, 0x400);

    e->burst_ntrigs = 1;
    e->L1A_per_loop = (nt+1) * (nb+1);
  }

  e->ConfigureVariable(ST_SEND_MASK, 1);
  e->ConfigureScan(ST_NMASK, 0, bins, 1);
  st_scan(burst_type, -1);
  if(e->RetrieveVariable(0, ST_CHIPSET) < 7) {
    e->ConfigureVariable(ST_NMASK, 128);
  } else {
    // ABC130 has 256 channels!
    e->ConfigureVariable(ST_NMASK, 256);
  }
  e->ConfigureVariable(ST_SEND_MASK, 0);

  // Restore
  if(trtype == 100) {
    // Restore, don't generate L1 too
    e->ConfigureVariable(ST_HSIO_REG_BASE + 19, 0x0);
  }
  e->RestoreState();
  e->CacheMasks(1);
}

void st_nmask_single(int burst_type, int ntrigs, int trtype) {
  e->SaveState();
  e->CacheMasks(0);

  e->burst_ntrigs = ntrigs;
  e->burst_trtype = trtype;
  e->do_cal_loop = 0;
  e->do_fits = 0;

  e->ConfigureVariable(ST_SEND_MASK, 1);
  e->ConfigureScan(ST_NMASK_SINGLE, 0, 127, 1);
  st_scan(burst_type, -1);
  e->ConfigureVariable(ST_NMASK, 128);
  e->ConfigureVariable(ST_SEND_MASK, 0);

  // Restore
  e->RestoreState();
  e->CacheMasks(1);
}

void st_rnmask(int burst_type, int ntrigs, int trtype) {
  e->SaveState();
  e->CacheMasks(0);

  e->burst_ntrigs = ntrigs;
  e->burst_trtype = trtype;
  e->do_cal_loop = 0;
  e->do_fits = 0;

  e->ConfigureVariable(ST_SEND_MASK, 1);
  e->ConfigureScan(ST_NMASK, 128, 0, 1);
  st_scan(burst_type, -1);
  e->ConfigureVariable(ST_NMASK, 128);
  e->ConfigureVariable(ST_SEND_MASK, 0);

  // Restore
  e->RestoreState();
  e->CacheMasks(1);
}

// Set and run single burst of the nmask scan
void st_nmask_burst(int burst_type, int ntrigs, int trtype, int n) {
  e->InitScan();
  e->SaveState();
  e->CacheMasks(0);

  e->burst_ntrigs = ntrigs;
  e->burst_trtype = trtype;
  e->do_cal_loop = 0;

  e->ConfigureVariable(ST_SEND_MASK,1);
  e->ConfigureVariable(ST_NMASK, n);
  e->ExecuteConfigs();

  e->burst_type = burst_type;
  e->ExecuteBurst(-1);
  st_display_burst();

  // Restore
  e->RestoreState();
  e->CacheMasks(1);
}

void st_doubleTriggerNoise() {
//  e->burst_ntrigs=100;
  e->L1A_per_loop = 2;
  e->ConfigureVariable(ST_NL1A, e->L1A_per_loop);
  e->burst_trtype = 53; // SR, CAL, Multi - L1A
  e->do_cal_loop = 0;
  e->throw_away = 1;

  e->ConfigureScan(ST_L1A_SPACING, 10, 20, 1);
  st_scan(8, -1);
  e->L1A_per_loop = 1;
  e->throw_away = 0;
}

void st_findPipeline() {
  e->burst_ntrigs=100;
  e->burst_trtype = 20;
  e->do_cal_loop = 0;

  e->ConfigureVariable(ST_SEND_MASK, 1);
  e->ConfigureScan(ST_PULSE_DELAY, 10, 228, 1);
  st_scan(8, -1);
}

#if 0
void st_abcn_test(){
  gROOT->ProcessLine(".L ABCNTest.cpp");
  ABCNTest();
}

void st_characterisation(){
  gROOT->ProcessLine(".L CharacterisationTest.cpp");
  CharacterisationTest();
}

void st_confirmation(){
  gROOT->ProcessLine(".L ConfirmationTest.cpp");
  ConfirmationTest();
}

void st_cltt(){
  gROOT->ProcessLine(".L HybridColdTest.cpp");
  HybridColdTest();
}

void st_eltt(){
  gROOT->ProcessLine(".L EndCapHLTT.cpp");
  EndCapHLTT();
}

void st_hltt(){
  gROOT->ProcessLine(".L HybridLTT.cpp");
  HybridLTT();
}

void st_mltt(){
  gROOT->ProcessLine(".L ModuleLTT.cpp");
  ModuleLTT();
}

void st_ivltt(){
  gROOT->ProcessLine(".L IVLTT.cpp");
  IVLTT();
}

void st_novt(){
  Int_t n_exec,delt;

  gROOT->ProcessLine(".L NOvsT.cpp");

  printf("How many measurements of the noise occupancy do you want (int)?\n");
  scanf("%d",&n_exec);
  printf("Specify the time to be waited between scans, in minutes (int):\n");
  scanf("%d",&delt);
  printf("You asked for %d minutes wait.\n",delt);

  NOvsT(n_exec,1000000,delt,1.,1,kTRUE);
}

void st_cn(){
  Int_t evt;
  Float_t qthr;
  gROOT->ProcessLine(".L CorrelNoise.cpp");
  printf("Enter number of events (int), please:");
  scanf("%d",&evt);
  printf("Enter threshold in fC (float) [-100 to leave unchanged]:");
  scanf("%f",&qthr);
  CorrelNoise(evt,qthr,1);
}

void st_gamma(){
  Int_t evt;
  Float_t qthr;
  gROOT->ProcessLine(".L Gamma.cpp");
  printf("Enter number of events (int), please:");
  scanf("%d",&evt);
  printf("Enter threshold in fC (float) [-100 to leave unchanged]:");
  scanf("%f",&qthr);
  Gamma(evt,qthr);
}

void st_inj(){
  gROOT->ProcessLine(".L FastInj.cpp");
  FastInj();
}

void st_multiinj(){
  gROOT->ProcessLine(".L MultiInj.cpp");
  Int_t nrep;
  Char_t label[128];
  printf("Enter number of interations WITH injection:   ");
  scanf("%d",&nrep);
  printf("Enter brief description for this measurement (no space chars:  ");
  scanf("%s",label);
  MultiInj(nrep,label);
}
#endif

void st_set_dbuser(){
  // Check, and if necessary prompt the user to enter,
  // the username to be used for DB upload.  PWP 21.03.02

  Char_t  string[32];

  Int_t length;
 
  printf("You are logged in as %s\n", gSystem->Getenv("USERNAME"));

  const Char_t* user = gSystem->Getenv("SCTDB_USER");

  if(user==NULL){
    printf("SCTDB_USER is undefined.\n\n");
    printf("You will now be asked to enter your user ID as stored in the SCT database.\n");
    printf("If you are logged on with your own personal computer account, you can skip\n");
    printf("this step by defining an environment variable SCTDB_USER equal to your DB\n");
    printf("identity (4 characters or less).\n\n");
    length = 10;
  }else{
    printf("SCTDB_USER is defined as %s\n",user);

    length = strlen(user);
    if(length>4){
      printf("\n  THIS IS TOO LONG (maximum four characters).\n");
      printf("  Please correct the setting of the environment variable SCTDB_USER!!\n\n");
      length=10;
    }else if(length<2){
      printf("\n  THIS IS TOO SHORT (minimum two characters).\n");
      printf("  Please correct the setting of the environment variable SCTDB_USER!!\n\n");
      length=10;
    }else{
      return;
    }
  }

  while(length>4){
    printf("Please enter the username you wish to use for DB upload\n");
    printf("(This should match your entry in the SCT database):");
    //GUY:ITSDAQchange
    //scanf("%s",string);
    strcpy(string,"BNL");
    printf("  I read \"%s\".\n",string);
    length = strlen(string);
    if(length>4){
      printf("\n  THIS IS TOO LONG (maximum four characters).\n");
      length = 10;
    }else if(length<2){
      printf("\n  THIS IS TOO SHORT (minimum two characters).\n");
      length = 10;
    }else{
      //printf("  Do you want this username to be used for DB upload? (0/1)\n");
      //scanf("%d",&length);
      //if(length==1){
        gSystem->Setenv("SCTDB_USER",string);
	//}else{
        //length=10;
	//}
    }
  }
}

void st_bell(){

  char string[64];

  if(strncmp(gSystem->GetName(),"Unix",4)!=0){
    if(e->abort==0){
      sprintf(string,"%sbin\\playwav end.wav", gSystem->WorkingDirectory());
    }else{
      sprintf(string,"%sbin\\playwav error.wav", gSystem->WorkingDirectory());
    }
    gSystem->Exec(string);
  }

}
