'''
pihat I2C buses from the multiplexer
---------------
              |__I2C  2
              |__0x06
              |
||------||-----
I2C     I2C
 1       3
0x05    0x07
'''
import smbus

class multiplex:
    def __init__(self,bus=1,address=0x73):
        self.bus=smbus.SMBus(bus)
        self.address=address
        
    def set_channel(self,channel):
        print(channel)
        if(channel==0):
            action=0x04
        elif(channel==1):
            action=0x05
        elif(channel==2):
            action=0x06
        elif(channel==3):
            action=0x07
        else:
            action=0x00
        self.bus.write_byte_data(self.address,0x04,action)

    def select_i2c(self,i2c):
        #i2c should be 1,2,3 labeled above
        self.set_channel(i2c)

    def set_adc(self):
        self.set_channel(0)
        
if __name__=="__main__":
    bus=1
    plexer=multiplex(1)
    plexer.set_channel(2)
