
import smbus
import time

class adc:
    def __init__(self,vref=2.8,bus=1,address=0x33):
        self.bus=smbus.SMBus(bus)
        self.address=address
        self.vref=vref

        #this comes from trial and error, not sure where it is on the data sheet
        #channels from left to right where the gpio pins are on the right
        self.channel_address=[41,43,45,47,39,37,35,33]
    def read_channel(self,channel):
        self.bus.write_byte(self.address,0x67) #open in read mode
        register_add=self.channel_address[channel]
        data=self.bus.read_i2c_block_data(self.address,register_add,2) 
        adc_value=data[0] & 15
        adc_value=adc_value << 8
        adc_value=adc_value | data [1]
        return adc_value

    def read_voltage(self,channel):
        adc_value=self.read_channel(channel)
        voltage=self.vref * adc_value/4096.0 #12 bit adc
        return voltage
