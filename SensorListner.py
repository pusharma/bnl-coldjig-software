import threading
import re
import zmq_client as zq
import zmq
import sys

if sys.version_info[0]>=3:
        from PyQt5 import QtCore
else:
        from PyQt4 import QtCore
        
class ServerThread(QtCore.QThread):
	sig = QtCore.pyqtSignal(str)
	
	def __init__(self,threadID):
		super(QtCore.QThread, self).__init__()
		self.threadID = threadID
		self.lock = threading.Lock()
		self.ntc=25.0
		self.ITSClient=zq.Client("127.0.0.1","5555")
		self.ColdJig=zq.Client("10.2.242.125","5556")
		self.StartTest=False
		self.TestTemp=25
	def run(self):
		print("Starting " + str(self.threadID))
		self.server()

	def server(self):
		port = "5554"
		context = zmq.Context()
		socket=context.socket(zmq.REP)
		socket.bind("tcp://*:%s" %port)
		while True:
			message = socket.recv()
			message=str(message)
			print("recieved message: ",message)
			if "break" in message.lower():
				socket.send("Goodbye!".encode())
				print("from listner: goodbye")
				break
			elif "ntc" in message.lower():
				self.ntc=float(re.findall(r"\d+\.*\d+",message)[0])
				if(self.StartTest and self.ntc<self.TestTemp):
                                        try:
                                                self.ITSClient.SendServerMessage("Start strobe delay")
                                                self.sig.emit("Launched ITSDAQ confirmation tests")
                                                self.StartTest=False
                                               
                                        except:
                                                pass
                                if(self.StartTest):
                                         self.sig.emit("NTC temp: "+str(self.ntc))
				socket.send("Set NTC temp".encode())
				
			elif "start test" in message.lower():
				print("starting confirmation test")
				self.TestTemp=float(re.findall(r"-*\d+\.*\d+",message)[0])
				self.StartTest=True
				self.sig.emit("Starting Test, at "+str(self.TestTemp))
				socket.send("starting confirmation test".encode())
				
			elif "finished test" in message:
				self.sig.emit("Test Finished")
				self.ColdJig.SendServerMessage("set chiller to 20")
				self.sig.emit("setting chiller to 20")
				socket.send("We are finishing the test".encode())
			else:
				socket.send("Good to go!".encode())
                
if __name__=="__main__":
        listner=ServerThread("Sensor Listner")
        listner.start()
