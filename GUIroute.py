import sys
sys.path.insert(0,"DCS")
from flask import Flask, render_template, request, jsonify, session, flash, abort
import time
import  hashlib
app = Flask(__name__)
from zmq_client import Client
import os
import socket
import DCS.ColdJigLib as  ColdJig
from subprocess import check_output
#from python_amac import amac

#module1=amac(0)
@app.route('/login', methods=['POST'])
def do_admin_login():
    pswd = request.form['password'].encode('utf-8')
    password = hashlib.sha256(pswd).hexdigest()
    with open('password.txt', 'r') as myfile:
        hash = myfile.read().replace('\n', '')
    if password == hash:
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return my_form()

@app.route("/logout", methods=['GET','POST'])
def logout():
    session['logged_in'] = False
    return my_form()

@app.route('/')
def my_form():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return render_template('my-form.html')

@app.route('/channel1', methods=['GET','POST'])
def set_channel1():
    volts = request.form['channel1']
    #print(volts)
    data = []
    try:
        reply = ColdJig.LV_SetVoltage(volts,1)
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array = data)

@app.route('/readchannel1', methods=['GET','POST'])
def read_channel1():
    data = []
    try:
        volt = ColdJig.LV_GetVoltage(1)
        print(volt)
        current = ColdJig.LV_GetCurrent(1)
        data = [1,volt, current,1]
    except IOError:
        data = [0]
    print(data)
    return jsonify(array = data)

@app.route('/channel2', methods=['GET','POST'])
def set_channel2():
    volts = request.form['channel2']
    #print(volts)
    data = []
    try:
        reply = ColdJig.LV_SetVoltage(volts,2)
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array = data)

@app.route('/readchannel2', methods=['GET','POST'])
def read_channel2():
    data = []
    try:
        volt = ColdJig.LV_GetVoltage(2)
        current = ColdJig.LV_GetCurrent(2)
        data = [1,volt, current,2]
    except IOError:
        data = [0]
    print(data)
    return jsonify(array = data)

@app.route('/channel3', methods=['GET','POST'])
def set_channel3():
    volts = request.form['channel3']
    print(volts)
    data = []
    try:
        reply = ColdJig.LV_SetVoltage(volts,3)
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array=data)

@app.route('/readchannel3', methods=['GET','POST'])
def read_channel3():
    data = []
    try:
        volt = ColdJig.LV_GetVoltage(3)
        current = ColdJig.LV_GetCurrent(3)
        data = [1,volt, current,3]
    except IOError:
        data = [0]
    print(data)
    return jsonify(array = data)

@app.route('/channel4', methods=['GET','POST'])
def set_channel4():
    volts = request.form['channel4']
    print(volts)
    data = []
    try:
        reply = ColdJig.LV_SetVoltage(volts,4)
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array=data)

@app.route('/readchannel4', methods=['GET','POST'])
def read_channel4():
    data = []
    try:
        volt = ColdJig.LV_GetVoltage(4)
        current = ColdJig.LV_GetCurrent(4)
        data = [1,volt, current,4]
    except IOError:
        data = [0]
    print(data)
    return jsonify(array = data)

@app.route('/channel6', methods=['GET','POST'])
def set_channel6():
    volts = request.form['channel6']
    print(volts)
    data = []
    try:
        reply = ColdJig.HV_SetVoltage(volts)
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array=data)

@app.route('/readchannel6', methods=['GET','POST'])
def read_channel6():
    data = []
    try:
        volt = ColdJig.HV_GetVoltage()
        current = ColdJig.HV_GetCurrent()
        data = [1,volt, current,1]
    except IOError:
        data = [0]
    print(data)
    return jsonify(array = data)


@app.route('/low', methods=['GET','POST'])
def low_voltage():
    if request.method == 'POST':
        result = request.form['power']
        reply = ""
        data = []
        try:
            if result == "On":
                reply = ColdJig.LV_TurnOn()
            elif result == "Off":
                reply = ColdJig.LV_TurnOff()
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array = data)

@app.route('/high', methods=['GET','POST'])
def high_voltage():
    if request.method == 'POST':
        result = request.form['power2']
        reply = ""
        data = []
        try:
            if result == "On":
                reply = ColdJig.HV_TurnOn()
            elif result == "Off":
                reply = ColdJig.HV_TurnOff()
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array=data)

@app.route('/chiller', methods=['GET','POST'])
def chiller_control():
    if request.method == 'POST':
        result = request.form['chillerpower']
        reply = ""
        data = []
        try:
            if result == "On":
                reply = ColdJig.Chiller_TurnOn()
            elif result == "Off":
                reply = ColdJig.Chiller_TurnOff()
                data = [1]
        except IOError:
            data = [0]
        return jsonify(array=data)
                                                    
@app.route('/IVscan', methods=['GET','POST'])
def iv_scan():
    data = []
    try:
        ColdJig.do_IVcurve()
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array = data)

@app.route('/temp', methods=['GET','POST'])
def temperature():
    if request.method == 'POST':
        temp = request.form['temperature']
        message = "chiller,set,temperature at " + temp
        data = []
        try:
            print(temp)
            reply = ColdJig.Chiller_SetTemperature(int(temp))
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array=data)

@app.route('/readtemp', methods=['GET','POST'])
def read_temp():
    if request.method == 'POST':
        message = "chiller,get,GetTemperature"
        data = []
        try:
            reply = ColdJig.Chiller_GetTemperature()
            data = [1,reply]
        except IOError:
            data = [0]
        return jsonify(array=data)

@app.route('/ping', methods=['GET','POST'])
def ping():
    if request.method == 'POST':
        ping = request.form['IP']
        data = []
        try:
            ColdJig.Ping()
            data = [1]
        except IOError:
            data = [0]
        print(ping)
        return jsonify(array = data)

@app.route('/sensor', methods=['GET','POST'])
def sensor_data():
    if request.method == 'POST':
        sensor = request.form['power3']
        reply = ""
        data = []
        try:
            if sensor == "On":
                reply = ColdJig.start_sensors()
            elif sensor == "Off":
                reply = ColdJig.stop_sensors()
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array = data)

@app.route('/modulesensor', methods=['GET','POST'])
def module_sensor_data():
    if request.method == 'POST':
        sensor = request.form['power4']
        reply = ""
        data = []
        try:
            if sensor == "On":
                reply = ITSDAQ.SendServerMessage("module sensors on")
            elif sensor == "Off":
                reply = ITSDAQ.SendServerMessage("module sensors off")
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array = data)

@app.route('/powersensor', methods=['GET','POST'])
def power_sensor_data():
    #module1.configure("python_amac/example_config.det")
    time.sleep(0.5)
    #module1.lv_on()
    time.sleep(0.5)
    if request.method == 'POST':
        sensor = request.form['power5']
        reply = ""
        data = []
        try:
            if sensor == "On":
                reply = ITSDAQ.SendServerMessage("power on")
            elif sensor == "Off":
                reply = ITSDAQ.SendServerMessage("power off")
            data = [1]
        except IOError:
            data = [0]
        return jsonify(array = data)

@app.route('/break', methods=['GET','POST'])
def break_connection():
    data = []
    try:
        ColdJig.stop_sensors()
        ColdJig.Thermal_Cycle("break")
        reply = ITSDAQ.SendServerMessage("break")
        data = [1]
    except IOError:
        data = [0]
    return jsonify(array = data)

@app.route('/startthermalcycle',methods=['GET','POST'])
def start_thermal_cycle():
    if request.method == 'POST':
        upper_temp=request.form['upper']
        lower_temp=request.form['lower']
        tests = request.form.getlist("tests")
        message="run_test "+" ".join([str(bl) for bl in tests])
        data = []
        try:
            print("browser started thermal cycle")
            if(int(upper_temp)<int(lower_temp)):
                print("bad temperatures")
                raise IOError
            print(message)
            ColdJig.Start_ThermalCycle(message,lower_temp,upper_temp)
            data = [1]
        except IOError:
            data=[0]
         
    return jsonify(array=data)

@app.route('/standardtest', methods=['GET','POST'])
def standard_test():
    if request.method == 'POST':
        data = []
        try:
            message="run_test "+" ".join(["true" for i in range(7)])
            ITSDAQ.SendServerMessage(message.encode())
            data = [1]
        except IOError:
            data = [0]
        print(result)
    return jsonify(array = data)

@app.route('/itsdaqsession', methods=['GET','POST'])
def itsdaq_session():
    if request.method == 'POST':
        sensor = request.form['itsdaq']
        reply = ""
        data = []
        try:
            reply = ITSDAQ.SendServerMessage("start_itsdaq")
            data = [1]
        except IOError:
            data = [0]
    return jsonify(array = data)
                                                    
@app.route('/interlock', methods=['GET','POST'])
def set_interlock():
    if request.method == 'POST':
        temp_before = request.form['temp before']
        temp_after = request.form['temp after']
        humidity_before = request.form['humidity before']
        humidity_after = request.form['humidity after']
        hybrid_before = request.form['hybrid before']
        hybrid_after = request.form['hybrid after']
        user_before = request.form['user before']
        user_after = request.form['user after']
        data = []
        try:
            reply1 = ColdJig.SendServerMessage(
                "interlock,sht,temp,range of " + temp_before + " to" + temp_after)
            reply2 = ColdJig.SendServerMessage(
                "interlock,sht,humidity,range of " + humidity_before + " to" + humidity_after)
            reply3 = ColdJig.SendServerMessage(
                "interlock,user ntc,range of " + hybrid_before + " to" + hybrid_after)
            reply4 = ColdJig.SendServerMessage(
                "interlock,hybrid ntc,range of " + user_before + " to" + user_after)
            result = reply1+"\n"+reply2+"\n"+reply3+"\n"+reply4
            data = [1]
            print(result)
        except IOError:
            data = [0]
        return jsonify(array = data)

def get_host():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    host=s.getsockname()[0]
    s.close()
    return host

def get_ip(ethernet=False):
    ips = check_output(['hostname', '--all-ip-addresses']).split(" ")
    host=ips[0] if ethernet else ips[1]
    return host

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    ITSDAQ = Client(ColdJig.master, "5555")
    Master = Client("127.0.0.1", "5556")
    app.run(host=get_ip(ethernet=True), port=5000,debug=True,use_reloader=False)

