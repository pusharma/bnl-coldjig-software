Directory structure
DCSDAQ- MasterGui.py- Implements Gui, currently using PyQt5, also now a Qt4 version
	SensorListner- Server loop, listens for data from rasberry pi
	zmq_client- Program to send data to other servers

    to run, do python MasterGui.py
    
DCS- Cold jig programs, meant to run on a rasberry pi.Contains:
     ColdJig.py- Main server, runs a server thread listning for instructions from Master and a thread running in continous loops that reads out sensors
     DCSConfig.ini - configuration file for Power Supplies and chiller (and interlock eventually)
     SensorConfig.ini -conifguration file for sensors
     initialize.py- reads out configuration files

     packages:
     LV-low voltage power suppply programs
     HV- High voltage power supply programs
     Chiller- chiller programs
     To add implementation of new power supply, add a .py program to the package witht the class
     and import it in the __init__.py file. 
     Interlock- currently only sort of runs cambridge interlock programs
     Sensors- Implementation of sensor readout, currently SHT75, NTCs, FLow meter and Data Logger
     
     To run just run ColdJig.py, 
     
DAQ-Put ITSDAQ main branch here, run ITSDAQserver.py which will listen and run the appropriate tests when called for (currently only strobe delay).


Dependecies:
MasterGui:
    PyQt4/5 (pick one, 4 works better on python 2 and 5 works better on python 3)
    zmq
    Threading
    Queue/queue (python 2/3 )
    
ColdJig:
    graphyte
    serial
    sht-sensor (if you use SHT75)
    Configparser/configparser (2/3)
    zmq
    Threading
    Queue/queue 




Installing Grafana:

-------------Debian------------------

documented fairley well here:
https://www.linode.com/docs/uptime/monitoring/how-to-install-graphite-and-grafana-on-ubuntu-14-04/
However, in the installing grafana section, instead of what they say follow the isntructions on the grafana website:
http://docs.grafana.org/installation/

I've reproduced the steps below:

Pre-requesit; python, django- tested with version 1.8.17

sudo apt-get install build-essential graphite-web graphite-carbon python-dev apache2 libapache2-mod-wsgi libpq-dev python-psycopg2
configure this file: /etc/carbon/storage-schemas.conf to your liking
sudo cp /usr/share/doc/graphite-carbon/examples/storage-aggregation.conf.example /etc/carbon/storage-aggregation.conf
sudo service carbon-cache start

(installing postresql)
sudo apt-get install postgresql
su - postgres
createuser graphite --pwprompt
createdb -O graphite graphite
createdb -O graphite grafana
su - your_user_name_here (switch back)

configure /etc/graphite/local_settings.py to your liking
sudo graphite-manage syncdb

*if you see an internal server error at some point do:
sudo graphite-manage migrate --settings=graphite.settings --run-syncdb

(apache)
sudo cp /usr/share/graphite-web/apache2-graphite.conf /etc/apache2/sites-available
edit the file /etc/apache2/sites-available/apache2-graphite.conf
     add the line <VirtualHost *:8080>
     Listen 80
     Listen 8080    

sudo a2dissite 000-default
sudo a2ensite apache2-graphite
sudo service apache2 reload

check localhost:8080, should see a graphite gui (mostly white page with some options)
if ok, install grafana:
http://docs.grafana.org/installation/
then do
sudo a2enmod proxy proxy_http xml2enc
edit the file /etc/apache2/sites-available/apache2-grafana.conf
     add the lines:
     <VirtualHost *:80>
       ProxyPreserveHost On
       ProxyPass / http://127.0.0.1:3000/
       ProxyPassReverse / http://127.0.0.1:3000/
       ServerName localhost
    </VirtualHost>

sudo a2ensite apache2-grafana
sudo update-rc.d grafana-server defaults 95 10
sudo service grafana-server start
sudo service apache2 restart

go to localhost, should see grafana.


----------Linux-CentOS 7-------------------

first install graphite,
follow instructions here
https://www.server-world.info/en/note?os=CentOS_7&p=graphite

make sure to change the retention rate in
/etc/carbon/storage-schemas.conf

check that it works by visiting http://server_ip_address
after graphite works make these changes:

in /etc/httpd/conf.d/graphite-web.conf
change the virtualhost port from *80 to *8080

in /etc/httd/conf/httpd.conf
add listen 8080 below the line listen 80

check that graphite works on port 8080
http://server_ip_address:8080

download grafana
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana-5.0.0-1.x86_64.rpm
sudo rpm -Uvh grafana-5.0.0-1.x86_64.rpm

start grafana:
sudo systemctl daemon-reload
sudo service grafana-server start

go to
http://ip_address:3000 
should see a grafana screen

to send data, you might have to disable a firewall (obviously use with caution)
systemctl disable firewalld



----------------Windows----------


Grafana for windows:
http://docs.grafana.org/installation/windows/
guide here:
https://www.youtube.com/watch?v=QJxk0V5i6qM


Since graphite (database backend) isn't for windows, I think the next best thing is influxDB form here:
https://github.com/influxdata/influxdb
Note at at BNL we use graphite, so I only have support for that, I will add influxDB support in the future. Anyway if someone is adventerous and is willing to try,guide here:
https://www.youtube.com/watch?v=ocCZkVDEyvQrequirements:

requirements:
go:https://golang.org/doc/install
git for windows
TortioseHg:https://tortoisehg.bitbucket.io/

Steps:
install the above software.
cd C:\Go
mkdir projects
set "GOPATH=C:\GO\projects"
cd projects
go get https://github.com/influxdata/influxdb
cd src\github.com\influxdata\influxdb
go get -u -f ./...
go build ./...
cd %gopath%\bin
influxd config > influxdb.generated.com
copy  influxdb.generated.com influxdb.conf
open influxdb.conf and change bind addresses from 8080 to localhost:8080
influxdb -config influxdb.conf



-------------Running Grafana---------------------
in a browser type in localhost:80 or 3000 and login default user/pass is admin/admin
go to data source
create new source
select type- graphite on linux, influxDB on windows
url should be localhost:8080
click save & test
      if source not found or some such, check that graphite is working by going
      to localhost:8080. If you see grafana on there, check that all the above steps are done correctly
      you most likley overwrote the graphite source on the 8080 port with something
      else like grafana. 

create a metric for testing:
open a console
start python interperter (type, 'python')
(in the python console)

import graphyte
       if not avaible, download with sudo pip install graphyte
graphyte.init('127.0.0.1')
graphyte.send('TestMetric',9) #or some random number, send a couple of them
graphyte.send('TestMetric',2)
graphyte.send('TestMetric',11)
graphyte.send('TestMetric',15)


in grafana go to new dashboard
select graph, click on the title, and go to edit
go to the metrics tab
in the data source (first line) select graphite 
in the query (labled 'A', 'B' etc) click on select metric and select 'TestMetric'
should now see data pop up on the screen 

-------TC 08---------------------------
to get tc08 working on raspberry pi:
https://www.picotech.com/support/topic14649.html
open the file
/etc/apt/sources.lists.d/picoscope.list (create it if it doesn't exist)
add the line deb http://labs.picotech.com/raspbian/ picoscope main
then on the command line do:
$ wget -O - http://labs.picotech.com/debian/dists/picoscope/Release.gpg.key | sudo apt-key add -

$ sudo apt-get update
sudo apt-get install libusbtc08
then I would build this package
